package com.itmck.ormgeneratortool;

import com.alibaba.fastjson.JSON;
import com.itmck.ormgeneratortool.dao.entity.LcProjectInfo;
import com.itmck.ormgeneratortool.service.GeneratorService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * 她似晚霞，愈晚愈浓
 * <p>
 * Create by M ChangKe 2022/7/31 10:23
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class ViewControllerTest {
    @Resource
    private GeneratorService generatorService;
    @Test
    public void run() {
        LcProjectInfo lcProjectInfo = new LcProjectInfo();
        lcProjectInfo
                .setGroupId("com.yx")//组织
                .setProjectName("my-first-demo")//应用名
                .setOpenSaToken(true)//是否使用sa-token鉴权
                .setCreatePre(true)//是否创建前端页面
                .setLoginPage(true)//是否创建登陆页面
                .setOpenSwagger(true)//是否开启后台swagger
                .setOverwrite(true)//是否覆盖代码
        ;
        ArrayList<String> strings = new ArrayList<>();
        strings.add("user");
        lcProjectInfo.setTables(strings);
        generatorService.createSimpleSystem(lcProjectInfo);
    }



    @Resource
    private DataSourceProperties dataSourceProperties;

    @Test
    public void queryCurrentTableList() {

        String url = dataSourceProperties.getUrl();

        log.info("{}", JSON.toJSONString(dataSourceProperties));

    }

}