package com.itmck.ormgeneratortool;

import com.alibaba.fastjson.JSON;
import com.itmck.ormgeneratortool.dao.entity.LcUser;
import com.itmck.ormgeneratortool.dao.mapper.LcUserMapper;
import com.itmck.ormgeneratortool.dto.LcUserInfoDTO;
import com.itmck.ormgeneratortool.util.MyQueryPageUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;


@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
class OrmGeneratorToolApplicationTests {

    @Resource
    private LcUserMapper lcUserMapper;

    @Test
    public void lcUserMapper() {

        LcUserInfoDTO lcUserInfoDTO = lcUserMapper.queryUserRoleAndPermissions("mck");
        System.out.println(JSON.toJSONString(lcUserInfoDTO));

        List<LcUser> ls = MyQueryPageUtil.builderFuncList(lcUserMapper, qp -> qp.eq(LcUser::getUserName, "mck")
                .list());
        System.out.println(ls);
    }


}
