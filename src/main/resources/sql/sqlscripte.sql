
create schema spring_db collate utf8_general_ci;

DROP TABLE IF EXISTS `lc_permission`;

CREATE TABLE `lc_permission` (
  `PERMISSION_ID` int NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `PERMISSION_NAME` varchar(20) DEFAULT NULL COMMENT '权限名',
  PRIMARY KEY (`PERMISSION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3003 DEFAULT CHARSET=utf8mb3 COMMENT='权限表表';


LOCK TABLES `lc_permission` WRITE;
INSERT INTO `lc_permission` (`PERMISSION_ID`, `PERMISSION_NAME`) VALUES (3001,'管理员权限'),(3002,'超管权限');
UNLOCK TABLES;



DROP TABLE IF EXISTS `lc_role`;
CREATE TABLE `lc_role` (
  `ROLE_ID` int NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `ROLE_NAME` varchar(20) DEFAULT NULL COMMENT '角色名',
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2003 DEFAULT CHARSET=utf8mb3 COMMENT='角色表';


LOCK TABLES `lc_role` WRITE;
INSERT INTO `lc_role` (`ROLE_ID`, `ROLE_NAME`) VALUES (2001,'管理员'),(2002,'超级管理员');
UNLOCK TABLES;


DROP TABLE IF EXISTS `lc_role_permission`;
CREATE TABLE `lc_role_permission` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `ROLE_ID` int DEFAULT NULL COMMENT '角色名',
  `PERMISSION_ID` int DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='角色权限关联表';

LOCK TABLES `lc_role_permission` WRITE;

INSERT INTO `lc_role_permission` (`id`, `ROLE_ID`, `PERMISSION_ID`) VALUES (1,2001,3001),(2,2002,3002);

UNLOCK TABLES;

DROP TABLE IF EXISTS `lc_table`;
CREATE TABLE `lc_table` (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3 COMMENT='代码生成业务表';

LOCK TABLES `lc_table` WRITE;
INSERT INTO `lc_table` (`table_id`, `table_name`, `table_comment`, `class_name`) VALUES (1,'user','用户表','User'),(6,'user1','用户表1','User1'),(7,'user1','用户表1','User1'),(8,'user1','用户表1','User1');
UNLOCK TABLES;


DROP TABLE IF EXISTS `lc_table_column`;

CREATE TABLE `lc_table_column` (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COMMENT='代码生成业务表字段';


LOCK TABLES `lc_table_column` WRITE;
INSERT INTO `lc_table_column` (`column_id`, `table_id`, `column_name`, `column_comment`, `column_type`, `java_type`, `java_field`, `is_pk`, `query_type`) VALUES (1,'1','name','姓名1','varchar','String','name',NULL,'EQ');
UNLOCK TABLES;

DROP TABLE IF EXISTS `lc_user`;

CREATE TABLE `lc_user` (
  `USER_ID` int NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `USER_NAME` varchar(20) DEFAULT NULL COMMENT '姓名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `status` int DEFAULT NULL COMMENT '状态 1:正常,2:锁定',
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8mb3 COMMENT='用户信息表';


LOCK TABLES `lc_user` WRITE;
INSERT INTO `lc_user` (`USER_ID`, `USER_NAME`, `password`, `status`) VALUES (1001,'mck','123',1);
UNLOCK TABLES;


DROP TABLE IF EXISTS `lc_user_role`;
CREATE TABLE `lc_user_role` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `USER_ID` int DEFAULT NULL COMMENT '权限名',
  `ROLE_ID` int DEFAULT NULL COMMENT '权限名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COMMENT='用户角色关联表';


LOCK TABLES `lc_user_role` WRITE;
INSERT INTO `lc_user_role` (`id`, `USER_ID`, `ROLE_ID`) VALUES (1,1001,2001),(2,1001,2002);

UNLOCK TABLES;

