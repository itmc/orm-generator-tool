package com.itmck.ormgeneratortool.util;

import cn.hutool.core.collection.CollectionUtil;
import com.google.common.collect.Maps;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * 她似晚霞，愈晚愈浓
 * <p>
 * Create by M 347451331@qq.com 2022/8/15 17:03
 * <p>
 * list转map
 **/
public final class MapUtil {
    private MapUtil() {
    }

    public static <V, K> Map<K, V> listToMap(List<V> list, Function<V, K> keyExtractor) {
        if (CollectionUtil.isEmpty(list)) {
            return Collections.emptyMap();
        }
        Map<K, V> res = Maps.newHashMap();
        for (V v : list) {
            K k = keyExtractor.apply(v);
            if (k == null) {
                continue;
            }
            res.put(k, v);
        }
        return res;
    }




    public static void main(String[] args) {

        List<String> list = Arrays.asList("mck", "wxp");
        Map<String, String> stringStringMap = MapUtil.listToMap(list, String::toString);
        System.out.println(stringStringMap);

    }
}