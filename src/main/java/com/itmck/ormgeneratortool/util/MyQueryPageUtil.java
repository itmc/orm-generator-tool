package com.itmck.ormgeneratortool.util;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itmck.ormgeneratortool.response.ApiResultPage;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/8/20 15:07
 **/
public class MyQueryPageUtil {

    /**
     * @param request request请求域
     * @param mapper  基础mapper
     * @param <T>     泛型
     * @return 返回泛型类分页
     */
    public static <V, T> ApiResultPage<V> buildPage(HttpServletRequest request, BaseMapper<T> mapper, Class<V> clazz) {

        Page<T> page = builder(request, mapper);
        List<V> collect = page.getRecords()
                .stream()
                .map(item -> BeanUtil.copyProperties(item, clazz))
                .collect(Collectors.toList());
        return ApiResultPage.ok(collect, page.getTotal());

    }

    /**
     * @param request request请求域
     * @param mapper  基础mapper
     * @param <T>     泛型
     * @return 返回泛型类分页
     */
    public static <T> Page<T> builder(HttpServletRequest request, BaseMapper<T> mapper) {

        return builder(request, mapper, null);
    }

    /**
     * @param request      request请求域
     * @param mapper       基础mapper
     * @param queryWrapper 查询mapper
     * @param <T>          泛型
     * @return 返回泛型类分页
     */
    public static <T> Page<T> builder(HttpServletRequest request, BaseMapper<T> mapper, QueryWrapper<T> queryWrapper) {

        long currentPage = 1L;
        long pageSize = 10L;
        if (ObjectUtil.isNull(queryWrapper)) {
            queryWrapper = new QueryWrapper<>();

        }
        Map<String, Object> requestParamsMap = RequestUtil.getRequestMap(request);
        for (Map.Entry<String, Object> entry : requestParamsMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (StrUtil.isNotBlank(key)) {
                if (StrUtil.equals("pageSize", key)) {
                    pageSize = Long.parseLong(String.valueOf(value));
                } else if (StrUtil.equals("currentPage", key)) {
                    currentPage = Long.parseLong(String.valueOf(value));
                } else {
                    queryConditionCheck(mapper, queryWrapper, key, value);
                }
            }
        }
        System.out.println(currentPage + "-------" + pageSize);
        return mapper.selectPage(new Page<>(currentPage, pageSize), queryWrapper);

    }

    public static <T> Page<T> builderFunc(HttpServletRequest request, BaseMapper<T> mapper, Consumer<QueryWrapper<T>> consumer) {
        long currentPage = 1L;
        long pageSize = 10L;
        QueryWrapper<T> qp = new QueryWrapper<>();
        consumer.accept(qp);
        Map<String, Object> requestParamsMap = RequestUtil.getRequestMap(request);
        for (Map.Entry<String, Object> entry : requestParamsMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (StrUtil.isNotBlank(key)) {
                if (StrUtil.equals("pageSize", key)) {
                    pageSize = Long.parseLong(String.valueOf(value));
                } else if (StrUtil.equals("currentPage", key)) {
                    currentPage = Long.parseLong(String.valueOf(value));
                } else {
                    queryConditionCheck(mapper, qp, key, value);
                }
            }
        }
        System.out.println(currentPage + "-------" + pageSize);
        return mapper.selectPage(new Page<>(currentPage, pageSize), qp);

    }


    public static <T> List<T> builderFuncList(BaseMapper<T> mapper, Function<LambdaQueryChainWrapper<T>,List<T>> consumer) {
        LambdaQueryChainWrapper<T> qp = new LambdaQueryChainWrapper<>(mapper);
        return consumer.apply(qp);

    }


    /**
     * 获取数据集合
     *
     * @param request request请求域
     * @param mapper  基础mapper
     * @param <T>     泛型类
     * @return 返回泛型类集合
     */
    public static <T> List<T> builderList(HttpServletRequest request, BaseMapper<T> mapper) {

        return builderList(request, mapper, null);

    }

    /**
     * 获取数据集合
     *
     * @param request      request请求域
     * @param mapper       基础mapper
     * @param queryWrapper 查询mapper
     * @param <T>          泛型类
     * @return 返回泛型类集合
     */
    public static <T> List<T> builderList(HttpServletRequest request, BaseMapper<T> mapper, QueryWrapper<T> queryWrapper) {

        if (ObjectUtil.isNull(queryWrapper)) {
            queryWrapper = new QueryWrapper<>();
        }
        Map<String, Object> requestParamsMap = RequestUtil.getRequestParamsMap(request);
        for (Map.Entry<String, Object> entry : requestParamsMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (ObjectUtil.isNotNull(value))
                queryConditionCheck(mapper, queryWrapper, key, value);
        }
        return mapper.selectList(queryWrapper);
    }


    /**
     * 前端使用get请求传入如  ?eq$name=mck&age=26
     * 对应当前条件查询支持 eq,like,likeLeft,likeRight,ge,le 默认为eq
     *
     * @param mapper       基础mapper
     * @param queryWrapper 查询mapper
     * @param key          eq$name=mck&age=26 的 name,age
     * @param value        eq$name=mck&age=26 的 mck,26
     * @param <T>          泛型
     */
    private static <T> void queryConditionCheck(BaseMapper<T> mapper, QueryWrapper<T> queryWrapper, String key, Object value) {

        String[] split = key.split("\\$");
        List<String> entityFieldList = ReflectUtil.getEntityFieldList(mapper.getClass());
        if (entityFieldList.contains(split[0])) {
            return;
        }

//        //判断是否是驼峰
//        String s = split[1];
//

        switch (split[0]) {
            case "in":
                queryWrapper.in(split[1], Arrays.asList(((String) value).split(",")));
                break;
            case "like":
                queryWrapper.like(split[1], value);
                break;
            case "likeLeft":
                queryWrapper.likeLeft(split[1], value);
                break;
            case "likeRight":
                queryWrapper.likeRight(split[1], value);
                break;
            case "ge":
                queryWrapper.ge(split[1], value);
                break;
            case "le":
                queryWrapper.le(split[1], value);
                break;
            case "eq":
            default:
                queryWrapper.eq(split[0], value);
                break;
        }
    }

}
