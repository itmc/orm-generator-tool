package com.itmck.ormgeneratortool.util;

import com.itmck.ormgeneratortool.constant.GenConstants;
import com.itmck.ormgeneratortool.dto.TemplateTable;
import com.itmck.ormgeneratortool.dto.TemplateTableColumn;
import org.apache.velocity.VelocityContext;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * 她似晚霞，愈晚愈浓
 * <p>
 * Create by M 347451331@qq.com 2022/8/4 15:18
 **/
public class VelocityUtils {

    public static VelocityContext getInstance() {
        return SingletonHolder.Instance;
    }
    private static class SingletonHolder {
        private static final VelocityContext Instance = new VelocityContext();
    }

    /**
     * 设置模板变量信息
     *
     * @return 模板列表
     */
    public static VelocityContext prepareContext(TemplateTable templateTable) {
        String moduleName = templateTable.getModuleName();
        String businessName = templateTable.getBusinessName();
        String packageName = templateTable.getPackageName();
        String functionName = templateTable.getFunctionName();
        VelocityContext velocityContext = VelocityUtils.getInstance();
        velocityContext.put("projectName", StringUtils.toCamelCaseMid(templateTable.getProjectName()).toLowerCase());
        velocityContext.put("projectNameFirstStrUpperCaseCamel", StringUtils.FirstStrUpperCase(StringUtils.toCamelCaseMid(templateTable.getProjectName())));
        velocityContext.put("groupId", templateTable.getGroupId());
        velocityContext.put("basePackage", getPackagePrefix(packageName));
        velocityContext.put("packageName", packageName);
        velocityContext.put("author", templateTable.getFunctionAuthor());

        velocityContext.put("loginPage", templateTable.isLoginPage());
        velocityContext.put("openSwagger", templateTable.isOpenSwagger());
        velocityContext.put("openSaToken", templateTable.isOpenSaToken());
        velocityContext.put("createPre", templateTable.isCreatePre());

        if (templateTable.isHaveTables()) {
            velocityContext.put("tableName", templateTable.getTableName());
            velocityContext.put("functionName", StringUtils.isNotEmpty(functionName) ? functionName : "【请填写功能名称】");
            velocityContext.put("ClassName", templateTable.getClassName());
            velocityContext.put("className", StringUtils.uncapitalize(templateTable.getClassName()));
            velocityContext.put("moduleName", templateTable.getModuleName());
            velocityContext.put("businessName", templateTable.getBusinessName());
            velocityContext.put("pkColumn", templateTable.getLcPkColumn());
            velocityContext.put("columns", templateTable.getColumns());
            velocityContext.put("moduleName", moduleName);
            velocityContext.put("businessName", businessName);
            velocityContext.put("importList", getImportList(templateTable));
            velocityContext.put("table", templateTable);
        }
        return velocityContext;
    }

    /**
     * 获取模板信息
     *
     * @return 模板列表
     */
    public static List<String> getTemplateList(TemplateTable templateTable) {
        List<String> templates = new ArrayList<>();
        if (templateTable.isCreatePre()) {
            templates.add("vm/views/editTable.html.vm");
            templates.add("vm/views/table.html.vm");
            templates.add("vm/views/about.html.vm");
            templates.add("vm/views/index.html.vm");
            templates.add("vm/views/self.html.vm");
        }

        if (templateTable.isLoginPage()) {
            templates.add("vm/views/login.html.vm");
            templates.add("vm/views/register.html.vm");
            templates.add("vm/lc/LoginController.java.vm");
            templates.add("vm/lc/LoginService.java.vm");
            templates.add("vm/lc/LoginServiceImpl.java.vm");
            templates.add("vm/lc/ViewController.java.vm");
            templates.add("vm/lc/LoginInterceptor.java.vm");
        }

        if (templateTable.isHaveTables()) {
            templates.add("vm/lc/controller.java.vm");
            templates.add("vm/lc/service.java.vm");
            templates.add("vm/lc/serviceImpl.java.vm");
            templates.add("vm/lc/domain.java.vm");
            templates.add("vm/lc/domainDto.java.vm");
            templates.add("vm/lc/mapper.java.vm");
            templates.add("vm/lc/mapper.xml.vm");
        }
        if (templateTable.isOpenSaToken()) {
            templates.add("vm/lc/SaTokenConfigure.java.vm");
            templates.add("vm/lc/StpInterfaceComp.java.vm");
        }
        templates.add("vm/lc/ApiResult.java.vm");
        templates.add("vm/lc/ApiResultPage.java.vm");
        templates.add("vm/lc/application.java.vm");
        templates.add("vm/lc/BusinessException.java.vm");
        templates.add("vm/lc/CommonEnum.java.vm");
        templates.add("vm/lc/EasyExcelUtil.java.vm");
        templates.add("vm/lc/GlobalExceptionAdvice.java.vm");
        templates.add("vm/lc/HttpFileUtil.java.vm");
        templates.add("vm/lc/MybatisPlusConfig.java.vm");
        templates.add("vm/lc/MyQueryPageUtil.java.vm");
        templates.add("vm/lc/ReflectUtil.java.vm");
        templates.add("vm/lc/RequestUtil.java.vm");
        templates.add("vm/lc/application.yml.vm");
        templates.add("vm/lc/applicationTest.java.vm");
        templates.add("vm/lc/pom.xml.vm");
        return templates;
    }


    /**
     * 获取包前缀
     *
     * @param packageName 包名称
     * @return 包前缀名称
     */
    public static String getPackagePrefix(String packageName) {
        int lastIndex = packageName.lastIndexOf(".");
        return StringUtils.substring(packageName, 0, lastIndex);
    }


    /**
     * 根据列类型获取导入包
     *
     * @param templateTable 业务表对象
     * @return 返回需要导入的包列表
     */
    public static HashSet<String> getImportList(TemplateTable templateTable) {
        List<TemplateTableColumn> columns = templateTable.getColumns();
        HashSet<String> importList = new HashSet<>();
        for (TemplateTableColumn column : columns) {
            if (!column.isSuperColumn() && GenConstants.TYPE_DATE.equals(column.getJavaType())) {
                importList.add("java.util.Date");
                importList.add("com.fasterxml.jackson.annotation.JsonFormat");
            } else if (!column.isSuperColumn() && GenConstants.TYPE_BIGDECIMAL.equals(column.getJavaType())) {
                importList.add("java.math.BigDecimal");
            }
        }
        return importList;
    }

}
