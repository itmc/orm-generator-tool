package com.itmck.ormgeneratortool.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 她似晚霞，愈晚愈浓
 * <p>
 * Create by M 347451331@qq.com 2022/7/29 17:36
 * <p>
 * 压缩算法类
 * 实现文件压缩，文件夹压缩，以及文件和文件夹的混合压缩
 **/
@Slf4j
public class FileZipUtil {


    /**
     * 压缩文件
     *
     * @param srcFile    源文件
     * @param targetFile 目标文件
     */
    public static void zipFiles(File srcFile, File targetFile) {

        try (
                FileOutputStream outputStream = new FileOutputStream(targetFile);
                ZipOutputStream out = new ZipOutputStream(outputStream)
        ) {
            if (srcFile.isFile()) {
                zipFile(srcFile, out, "");
            } else {
                File[] list = srcFile.listFiles();
                if (list != null && list.length > 0) {
                    for (File file : list) {
                        compress(file, out, "");
                    }
                }
            }
            System.out.println("压缩完毕");
        } catch (Exception e) {
            log.error("文件压缩失败", e);
        }
    }

    /**
     * 压缩单个文件
     *
     * @param srcFile 源文件
     * @param out     zip输出流
     * @param basedir 目标
     */
    public static void zipFile(File srcFile, ZipOutputStream out, String basedir) {
        if (!srcFile.exists())
            return;

        byte[] buf = new byte[1024];
        FileInputStream in = null;
        ZipEntry zipEntry = new ZipEntry(basedir + srcFile.getName());
        try {
            int len;
            in = new FileInputStream(srcFile);
            out.putNextEntry(zipEntry);
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null)
                    out.closeEntry();
                if (in != null)
                    in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 压缩文件夹里的文件
     * 起初不知道是文件还是文件夹--- 统一调用该方法
     *
     * @param file
     * @param out
     * @param basedir
     */
    private static void compress(File file, ZipOutputStream out, String basedir) {
        /* 判断是目录还是文件 */
        if (file.isDirectory()) {
            zipDirectory(file, out, basedir);
        } else {
            zipFile(file, out, basedir);
        }
    }


    /**
     * 压缩文件夹
     */
    public static void zipDirectory(File dir, ZipOutputStream out, String basedir) {
        if (!dir.exists()) {
            return;
        }
        File[] files = dir.listFiles();
        if (files != null && files.length > 0) {
            for (File file : files) {
                compress(file, out, basedir + dir.getName() + "/");
            }
        }
    }
}