package com.itmck.ormgeneratortool.util;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.support.ExcelTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;


/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2022/7/6 11:56
 **/
@Slf4j
public class EasyExcelUtil {

    /**
     * 同步无模型读（指定sheet和表头占的行数）
     *
     * @param inputStream
     * @param sheetNo     sheet页号，从0开始
     * @param headRowNum  表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static List<Map<Integer, String>> syncRead(InputStream inputStream, Integer sheetNo, Integer headRowNum) {
        return EasyExcelFactory
                .read(inputStream)
                .sheet(sheetNo)
                .headRowNumber(headRowNum)
                .doReadSync();
    }

    /**
     * 同步无模型读（指定sheet和表头占的行数）
     *
     * @param file
     * @param sheetNo    sheet页号，从0开始
     * @param headRowNum 表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static List<Map<Integer, String>> syncRead(File file, Integer sheetNo, Integer headRowNum) {
        return EasyExcelFactory.read(file).sheet(sheetNo).headRowNumber(headRowNum).doReadSync();
    }


    /**
     * 同步按模型读忽略表头（指定sheet和表头占的行数）
     *
     * @param inputStream
     * @param clazz       模型的类类型（excel数据会按该类型转换成对象）
     */
    public static <T> List<T> syncReadModel(InputStream inputStream, Class<T> clazz) {
        return EasyExcelUtil.syncReadModel(inputStream, clazz, 0, 1);
    }

    /**
     * 同步按模型读（指定sheet和表头占的行数）
     *
     * @param inputStream
     * @param clazz       模型的类类型（excel数据会按该类型转换成对象）
     * @param sheetNo     sheet页号，从0开始
     * @param headRowNum  表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static <T> List<T> syncReadModel(InputStream inputStream, Class<T> clazz, Integer sheetNo, Integer headRowNum) {
        return EasyExcelFactory
                .read(inputStream)
                .sheet(sheetNo)
                .headRowNumber(headRowNum)
                .head(clazz)
                .doReadSync();
    }

    /**
     * 同步按模型读（指定sheet和表头占的行数）
     *
     * @param file
     * @param clazz      模型的类类型（excel数据会按该类型转换成对象）
     * @param sheetNo    sheet页号，从0开始
     * @param headRowNum 表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static <T> List<T> syncReadModel(File file, Class<T> clazz, Integer sheetNo, Integer headRowNum) {
        return EasyExcelFactory
                .read(file)
                .sheet(sheetNo)
                .headRowNumber(headRowNum)
                .head(clazz).doReadSync();
    }


    /**
     * 异步无模型读（指定sheet和表头占的行数）
     *
     * @param inputStream
     * @param excelListener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     */
    public static void asyncRead(InputStream inputStream, ReadListener<T> excelListener) {
        EasyExcelUtil.asyncRead(inputStream, excelListener, 0, 1);
    }


    /**
     * 异步无模型读（指定sheet和表头占的行数）
     *
     * @param inputStream
     * @param excelListener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param sheetNo       sheet页号，从0开始
     * @param headRowNum    表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static void asyncRead(InputStream inputStream, ReadListener<T> excelListener, Integer sheetNo, Integer headRowNum) {
        EasyExcelFactory
                .read(inputStream, excelListener)
                .sheet(sheetNo)
                .headRowNumber(headRowNum)
                .doRead();
    }


    /**
     * 异步无模型读（指定sheet和表头占的行数）
     *
     * @param file
     * @param excelListener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param sheetNo       sheet页号，从0开始
     * @param headRowNum    表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static void asyncRead(File file, ReadListener<T> excelListener, Integer sheetNo, Integer headRowNum) {
        EasyExcelFactory.read(file, excelListener)
                .sheet(sheetNo)
                .headRowNumber(headRowNum)
                .doRead();
    }


    /**
     * 异步按模型读取, sheet页号，从0开始,读取数据(排除第一行表头)
     *
     * @param inputStream
     * @param excelListener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param clazz         模型的类类型（excel数据会按该类型转换成对象）
     * @param <T>
     */
    public static <T> void asyncReadModel(InputStream inputStream, ReadListener<T> excelListener, Class<T> clazz) {
        EasyExcelUtil.asyncReadModel(inputStream, excelListener, clazz, 0, 1);
    }


    /**
     * 异步按模型读取
     *
     * @param inputStream
     * @param excelListener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param clazz         模型的类类型（excel数据会按该类型转换成对象）
     * @param sheetNo       sheet页号，从0开始
     * @param headRowNum    表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static <T> void asyncReadModel(InputStream inputStream, ReadListener<T> excelListener, Class<T> clazz, Integer sheetNo, Integer headRowNum) {
        EasyExcelFactory.read(inputStream, clazz, excelListener)
                .sheet(sheetNo)
                .headRowNumber(headRowNum)
                .doRead();
    }


    /**
     * 异步按模型读取
     *
     * @param file
     * @param excelListener 监听器，在监听器中可以处理行数据LinkedHashMap，表头数据，异常处理等
     * @param clazz         模型的类类型（excel数据会按该类型转换成对象）
     * @param sheetNo       sheet页号，从0开始
     * @param headRowNum    表头占的行数，从0开始（如果要连表头一起读出来则传0）
     */
    public static <T> void asyncReadModel(File file, ReadListener<T> excelListener, Class<T> clazz, Integer sheetNo, Integer headRowNum) {
        EasyExcelFactory.read(file, clazz, excelListener)
                .sheet(sheetNo)
                .headRowNumber(headRowNum)
                .doRead();
    }

    /**
     * 导出Excel到文件
     *
     * @param filePath  文件地址
     * @param clazz     泛型类
     * @param data      待导出数据列表
     * @param sheetName sheetName
     */
    public static <T> void write(String filePath, Class<T> clazz, List<T> data, String sheetName) {
        EasyExcel.write(filePath, clazz)
                .sheet(sheetName)
                .doWrite(data);
    }


    /**
     * 导出excel到response响应流
     *
     * @param response       response响应流
     * @param clazz          泛型类
     * @param data           待导出数据列表
     * @param exportFileName 导出的excel文件名
     * @param sheetName      sheetName
     * @param <T>            泛型
     */
    public static <T> void writeWeb(HttpServletResponse response, Class<T> clazz, List<T> data, String exportFileName, String sheetName) {

        response.setContentType("application/vnd.ms-excel" );
        response.setCharacterEncoding("utf-8" );
        try (ServletOutputStream outputStream = response.getOutputStream()) {
            // 这里URLEncoder.encode可以防止中文乱码
            String fileName = URLEncoder.encode(exportFileName, "UTF-8" );
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ExcelTypeEnum.XLSX.getValue());
            EasyExcel.write(outputStream, clazz)
                    .sheet(sheetName)
                    .doWrite(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
