package com.itmck.ormgeneratortool.util;

public interface FileDirUtil {

    String JAVAFILE = "/src/main/java/";
    String JAVA_TEST = "/src/test/java/";
    String COMPONENT = "/component";
    String CONFIG = "/config";
    String CONSTANT = "/constant";
    String DAO = "/dao";
    String ENTITY = "/dao/entity";
    String MAPPER = "/dao/mapper";
    String CONTROLLER = "/controller";
    String SERVICE = "/service";
    String SERVICE_IMPL = "/service/impl";
    String DTO = "/dto";
    String ENUMS = "/enums";
    String EXCEPTION = "/exception";
    String REQUEST = "/request";
    String RESPONSE = "/response";
    String UTIL = "/util";
    String RESOURCES = "/src/main/resources";
    String MAPPER_XML = "/mybatis/mapper";
    String STATICDIR = "/static";
    String TEMPLATES = "/templates";
    String ZIP = ".zip";
}
