package com.itmck.ormgeneratortool.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.UUID;


/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2022/7/15 14:03
 * <p>
 * http上传下载文件工具类
 **/
@Slf4j
public class HttpFileUtil {


    /**
     * 上传文件
     *
     * @param files
     * @param desPath
     */
    public static void upload(List<MultipartFile> files, String desPath) {
        if (null == desPath) {
            throw new RuntimeException("文件地址不能为空" );
        }
        File f = new File(desPath);
        if (!f.exists()) {
            boolean mkdir = f.mkdir();
            if (!mkdir) {
                throw new RuntimeException("创建文件夹失败" );
            }
        }
        files.forEach(file -> {
            //获取文件类型
            int dot = Objects.requireNonNull(file.getOriginalFilename()).lastIndexOf('.');
            if ((dot > -1) && (dot < (file.getOriginalFilename().length()))) {
                String fileType = file.getOriginalFilename().substring(dot + 1);
                log.info("当前文件类型:{}", fileType);
            }

            String fileName = UUID.randomUUID() + file.getOriginalFilename();
            try {
                file.transferTo(new File(desPath + File.separatorChar + fileName));
                log.info("上传结束：" + fileName);
            } catch (IOException e) {
                throw new RuntimeException("文件上传失败" );
            }
        });
    }


    /**
     * 通过浏览器进行下载
     *
     * @param url 文件地址
     * @param response 浏览器响应流
     * @param fileName 文件名
     * @return void
     */
    public static void downLoadFile(String url, HttpServletResponse response, String fileName) {

        File file = new File(url);
        if (!file.exists()) {
            throw new RuntimeException("文件不存在" );
        }
        response.setContentType("application/octet-stream" );
        response.setHeader("content-disposition", "attachment;fileName=" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8));
        try (
                FileInputStream fis = new FileInputStream(file);
                ServletOutputStream os = response.getOutputStream()
        ) {
            byte[] bytes = new byte[1024];
            int len;
            while ((len = fis.read(bytes)) != -1) {
                os.write(bytes, 0, len);
            }
            os.flush();
        } catch (IOException e) {
            log.error("下载失败", e);
            throw new RuntimeException("下载失败" );
        }
    }


    /**
     * 将字符串输出到文件并通过浏览器进行下载
     *
     * @param contentStr 字符串
     * @param response   响应流
     * @param fileName   文件名
     * @return
     */
    public static void downStringToFile(String contentStr, HttpServletResponse response, String fileName) {

        if (StringUtils.isEmpty(contentStr)) {
            throw new RuntimeException("contentStr 为空" );
        }

        response.setContentType("application/octet-stream" );
        response.setHeader("Content-Disposition", "attachment;fileName" + new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8));
        try (
                InputStream fis = new ByteArrayInputStream(contentStr.getBytes());
                ServletOutputStream os = response.getOutputStream()
        ) {
            byte[] bytes = new byte[1024];
            int len;
            while ((len = fis.read(bytes)) != -1) {
                os.write(bytes, 0, len);
            }
            os.flush();
        } catch (IOException e) {
            log.error("下载失败", e);
            throw new RuntimeException("下载失败" );
        }
    }

}
