package com.itmck.ormgeneratortool.exception;


import com.itmck.ormgeneratortool.enums.CommonEnum;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/3 10:24
 **/
public class BusinessException extends RuntimeException {

    private String code;


    public BusinessException(CommonEnum commonEnum) {
        super(commonEnum.getDescription());
        this.code = commonEnum.getCode();
    }

    public BusinessException(String message, String code) {
        super(message);
        this.code = code;
    }

    public BusinessException(String message) {
        super(message);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}