package com.itmck.ormgeneratortool.exception;

import cn.dev33.satoken.exception.NotPermissionException;
import cn.dev33.satoken.exception.NotRoleException;
import com.itmck.ormgeneratortool.response.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/3 10:33
 * <p>
 * 自定义全局异常处理器
 **/
@Slf4j
@ControllerAdvice
public class GlobalExceptionAdvice {


    @ExceptionHandler(value = NullPointerException.class)
    @ResponseBody
    public ApiResult<String> responseException(NullPointerException exception) {
        log.error("空指针异常", exception);
        return ApiResult.error("空指针异常", "500");
    }


    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public ApiResult<String> businessHandler(BusinessException exception) {
        log.error("业务异常", exception);
        return ApiResult.error(exception.getMessage(), exception.getCode());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ApiResult<String> defaultHandler(Exception exception) {
        log.error("系统异常", exception);
        return ApiResult.error("空指针异常", "500");
    }

    @ExceptionHandler(value = NotPermissionException.class)
    @ResponseBody
    public ApiResult<String> defaultHandler(NotPermissionException exception) {
        log.error("权限校验失败", exception);
        return ApiResult.error("权限校验不通过", "600");
    }

    @ExceptionHandler(value = NotRoleException.class)
    @ResponseBody
    public ApiResult<String> defaultHandler(NotRoleException exception) {
        log.error("角色校验失败", exception);
        return ApiResult.error("角色校验不通过", "600");
    }

//    @ExceptionHandler(Exception.class)
//    public Object handleException(Exception e, HttpServletRequest req){
//        //使用HttpServletRequest中的header检测请求是否为ajax, 如果是ajax则返回json, 如果为非ajax则返回view(即ModelAndView)
//        String contentTypeHeader = req.getHeader("Content-Type");
//        String acceptHeader = req.getHeader("Accept");
//        String xRequestedWith = req.getHeader("X-Requested-With");
//        if ((contentTypeHeader != null && contentTypeHeader.contains("application/json"))
//                || (acceptHeader != null && acceptHeader.contains("application/json"))
//                || "XMLHttpRequest".equalsIgnoreCase(xRequestedWith)) {
//            return ApiResult.error(e.getMessage(),((BusinessException) e).getCode());
//        } else {
//            ModelAndView modelAndView = new ModelAndView();
//            modelAndView.addObject("msg", e.getMessage());
//            modelAndView.addObject("url", req.getRequestURL());
//            modelAndView.addObject("stackTrace", e.getStackTrace());
//            modelAndView.setViewName("error");
//            return modelAndView;
//        }
//    }
}