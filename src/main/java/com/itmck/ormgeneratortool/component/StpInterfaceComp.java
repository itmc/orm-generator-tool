package com.itmck.ormgeneratortool.component;

import cn.dev33.satoken.stp.StpInterface;
import com.itmck.ormgeneratortool.dao.mapper.LcUserMapper;
import com.itmck.ormgeneratortool.dto.LcPermissionDTO;
import com.itmck.ormgeneratortool.dto.LcRoleDTO;
import com.itmck.ormgeneratortool.dto.LcUserInfoDTO;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2022/7/18 17:44
 * <p>
 * 通过实现saToken内置接口实现权限与角色的管理
 **/
@Component
public class StpInterfaceComp implements StpInterface {


    @Resource
    private LcUserMapper lcUserMapper;


    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        LcUserInfoDTO lcUserInfoDTO = lcUserMapper.queryUserRoleAndPermissions((String) loginId);
        return lcUserInfoDTO
                .getRoles()
                .stream()
                .map(LcRoleDTO::getRoleName)
                .collect(Collectors.toList());

    }

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        LcUserInfoDTO lcUserInfoDTO = lcUserMapper.queryUserRoleAndPermissions((String) loginId);
        List<LcPermissionDTO> permissions = lcUserInfoDTO.getPermissions();
        return permissions.stream()
                .map(LcPermissionDTO::getPermissionName)
                .collect(Collectors.toList());
    }
}
