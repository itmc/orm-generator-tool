package com.itmck.ormgeneratortool.component;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

/**
 * 太阳当空照，花儿对我笑
 * <p>
 * Create by M ChangKe 2022/7/27 22:21
 *
 * mybatis-plus对象属性填充。这里是填充新增时候的status为1。实体参考：com.itmck.ormgeneratortool.dao.entity.LcUser
 *
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        this.fillStrategy(metaObject, "status", 1);
    }

    @Override
    public void updateFill(MetaObject metaObject) {

    }
}