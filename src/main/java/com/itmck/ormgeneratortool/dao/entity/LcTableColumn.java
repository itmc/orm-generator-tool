package com.itmck.ormgeneratortool.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * 她似晚霞，愈晚愈浓
 * <p>
 * Create by M 347451331@qq.com 2022/8/3 10:17
 **/
@Data
@Accessors(chain = true)
@TableName("LC_TABLE_COLUMN")
public class LcTableColumn {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Long columnId;
    /**
     * 归属表编号
     */
    private String tableId;
    /**
     * 列名称
     */
    private String columnName;
    /**
     * 列描述
     */
    private String columnComment;
    /**
     * 列类型
     */
    private String columnType;
    /**
     * JAVA类型
     */
    private String javaType;
    /**
     * JAVA字段名
     */
    private String javaField;
    /**
     * 是否主键（1是）
     */
    private String isPk;
    /**
     * 查询方式（等于、不等于、大于、小于、范围）
     */
    private String queryType;

}