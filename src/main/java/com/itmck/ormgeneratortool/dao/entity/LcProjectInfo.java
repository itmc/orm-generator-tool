package com.itmck.ormgeneratortool.dao.entity;


import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * 她似晚霞，愈晚愈浓
 * <p>
 * Create by M 347451331@qq.com 2022/7/29 14:17
 * <p>
 * 项目按照 groupId.projectName.packageName 去进行设置包
 **/
@Data
@Accessors(chain = true)
public class LcProjectInfo {


    /**
     * 项目名
     */
    private String projectName;

    /**
     * 组织
     */
    private String groupId;

    /**
     * 作者
     */
    private String author;

    /**
     * 是否覆盖生成文件
     */
    private boolean overwrite;
    private String overwriteStr;

    /**
     * 是否创建前端页面
     */
    private boolean createPre;
    private String createPreStr;


    /**
     * 是否创建登录页面
     */
    private boolean loginPage;
    private String loginPageStr;


    /**
     * 是否使用swagger
     */
    private boolean openSwagger;
    private String openSwaggerStr;


    /**
     * 是否使用sa-token
     */
    private boolean openSaToken;
    private String openSaTokenStr;


    private String tableStr;

    /**
     * 待创建的表集合
     */
    private List<String> tables = new ArrayList<>();
}
