package com.itmck.ormgeneratortool.dao.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

@Data
@TableName("LC_USER")
public class LcUser {


    /**
     * 用户id
     */
    @TableId(value = "USER_ID", type = IdType.AUTO)
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;


    /**
     * 状态 1:正常,2:锁定
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer status;

}
