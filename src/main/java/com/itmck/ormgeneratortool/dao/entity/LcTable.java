package com.itmck.ormgeneratortool.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务表 table_info
 *
 * @author ruoyi
 */
@Data
@TableName("LC_TABLE")
public class LcTable {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Long tableId;

    /**
     * 表名称
     */

    private String tableName;

    /**
     * 表描述
     */
    private String tableComment;


    /**
     * 实体类名称
     */
    private String className;


}