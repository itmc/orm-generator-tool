package com.itmck.ormgeneratortool.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 用户与角色关联表
 */
@Data
@TableName("LC_USER_ROLE")
public class LcUserRole {

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;


    /**
     * 用户id
     */
    private Long userId;


    /**
     * 角色id
     */
    private Long roleId;
}
