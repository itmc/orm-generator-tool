package com.itmck.ormgeneratortool.dao.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("information_schema.columns")
public class ColumnInformation {


    /**
     * field名
     */
    @TableField("columnName")
    private String columnName;
    /**
     * 类型
     */
    @TableField("dataType")
    private String dataType;

    /**
     * 注释
     */
    @TableField("columnComment")
    private String columnComment;

}
