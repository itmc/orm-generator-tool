package com.itmck.ormgeneratortool.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 角色与权限关联表
 */
@Data
@TableName("LC_ROLE_PERMISSION")
public class LcRolePermission {

    /**
     * 用户id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 角色id
     */
    private Long roleId;


    /**
     * 用户id
     */
    private Long permissionId;

}
