package com.itmck.ormgeneratortool.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 权限表
 */
@Data
@TableName("LC_PERMISSION")
public class LcPermission {

    /**
     * 角色
     */
    @TableId(value = "PERMISSION_ID", type = IdType.AUTO)
    private Long permissionId;

    /**
     * 角色名
     */
    private String permissionName;
}
