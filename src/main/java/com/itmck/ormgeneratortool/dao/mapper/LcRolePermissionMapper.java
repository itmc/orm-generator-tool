package com.itmck.ormgeneratortool.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.ormgeneratortool.dao.entity.LcPermission;
import com.itmck.ormgeneratortool.dao.entity.LcRolePermission;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LcRolePermissionMapper extends BaseMapper<LcRolePermission> {
}
