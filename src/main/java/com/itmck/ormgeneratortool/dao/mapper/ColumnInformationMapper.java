package com.itmck.ormgeneratortool.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.ormgeneratortool.dao.entity.ColumnInformation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ColumnInformationMapper extends BaseMapper<ColumnInformation> {

    @Select(value = "select COLUMN_NAME, DATA_TYPE, COLUMN_COMMENT from information_schema.COLUMNS where table_name = #{tableName} and table_schema = #{dbName}")
    List<ColumnInformation> showColumns(@Param("tableName") String tableName, @Param("dbName") String dbName);
}
