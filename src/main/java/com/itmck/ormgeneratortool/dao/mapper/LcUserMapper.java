package com.itmck.ormgeneratortool.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.ormgeneratortool.dao.entity.LcTable;
import com.itmck.ormgeneratortool.dao.entity.LcUser;
import com.itmck.ormgeneratortool.dto.LcUserInfoDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface LcUserMapper extends BaseMapper<LcUser> {
    /**
     * 查询用户的角色以及权限信息
     *
     * @param uerName 用户名
     * @return 用户的角色以及权限信息
     */
    LcUserInfoDTO queryUserRoleAndPermissions(@Param("uerName") String uerName);
}
