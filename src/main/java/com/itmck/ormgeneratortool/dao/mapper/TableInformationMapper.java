package com.itmck.ormgeneratortool.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.ormgeneratortool.dao.entity.TableInformation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface TableInformationMapper extends BaseMapper<TableInformation> {

    @Select(value = "select TABLE_NAME,TABLE_COMMENT from information_schema.TABLES where table_schema = #{tableSchema} and table_name =#{tableName}")
    TableInformation showTableInformation(@Param("tableName") String tableName, @Param("tableSchema") String tableSchema);

    @Select(value = "select TABLE_NAME,TABLE_COMMENT from information_schema.TABLES where table_schema = #{tableSchema}")
    List<TableInformation> showTableInformationList(@Param("tableSchema") String tableSchema);

    @Select(value = "select distinct table_schema from  information_schema.TABLES ")
    List<String> showTableSchemaList();

}
