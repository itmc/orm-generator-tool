package com.itmck.ormgeneratortool.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.ormgeneratortool.dao.entity.LcTable;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

@Mapper
public interface LcTableMapper extends BaseMapper<LcTable> {
    int insertLcTable(@Param("lcTable") LcTable lcTable);
}
