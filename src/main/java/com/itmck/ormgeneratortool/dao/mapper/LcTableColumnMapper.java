package com.itmck.ormgeneratortool.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.ormgeneratortool.dao.entity.LcTable;
import com.itmck.ormgeneratortool.dao.entity.LcTableColumn;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LcTableColumnMapper extends BaseMapper<LcTableColumn> {
}
