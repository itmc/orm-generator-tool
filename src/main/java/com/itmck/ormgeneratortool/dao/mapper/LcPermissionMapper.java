package com.itmck.ormgeneratortool.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.ormgeneratortool.dao.entity.LcPermission;
import com.itmck.ormgeneratortool.dao.entity.LcUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LcPermissionMapper extends BaseMapper<LcPermission> {
}
