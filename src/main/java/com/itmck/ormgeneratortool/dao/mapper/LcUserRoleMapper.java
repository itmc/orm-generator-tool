package com.itmck.ormgeneratortool.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.ormgeneratortool.dao.entity.LcUser;
import com.itmck.ormgeneratortool.dao.entity.LcUserRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LcUserRoleMapper extends BaseMapper<LcUserRole> {
}
