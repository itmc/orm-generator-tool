package com.itmck.ormgeneratortool.config;

import cn.dev33.satoken.interceptor.SaAnnotationInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 太阳当空照，花儿对我笑
 * <p>
 * Create by M ChangKe 2022/7/27 22:30
 * <p>
 * <p>
 * sa-token拦截器，进行权限以及角色的拦截。通过实现WebMvcConfigurer重写addInterceptors 增加拦截器
 */
@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {


    //定义登陆需要放行的接口，暴露登陆接口，注册接口
    private final String[] indexExcludes = new String[]{
            "/login.html",
            "/register.html",
            "/user/doLogin",
            "/user/register",
            "/static/**",
            "/bg.jpg"
    };

    //定义排除swagger访问的路径配置，通过放行不拦截swagger接口
    private final String[] swaggerExcludes = new String[]{
            "/swagger-ui.html/**",
            "/swagger-resources/**",
            "/webjars/**",
            "/v2/**",
            "/static/**",
            "/META-INF/resources/",
            "/META-INF/resources/webjars/"};


    // 注册Sa-Token的注解拦截器，打开注解式鉴权功能
    @Override
    public void addInterceptors(InterceptorRegistry registry) {


//        //添加登陆拦截器
//        registry.addInterceptor(new LoginInterceptor())
//                .addPathPatterns("/**")
//                .excludePathPatterns(indexExcludes)
//                .excludePathPatterns(swaggerExcludes);
//        // 注册注解拦截器，并排除不需要注解鉴权的接口地址 (与登录拦截器无关),拦截sa-token注解标记的method
//        registry.addInterceptor(new SaAnnotationInterceptor())
//                .addPathPatterns("/**")
//                .excludePathPatterns(indexExcludes)
//                .excludePathPatterns(swaggerExcludes);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("")
                .setViewName("forward:index");
    }


    /**
     * 配置静态资源过滤
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**")
                .addResourceLocations("classpath:/static/");
    }
}
