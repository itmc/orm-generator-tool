package com.itmck.ormgeneratortool.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 读取代码生成相关配置
 * <p>
 * Create by M ChangKe 2022/7/14 11:26
 **/
@Data
@Configuration
@ConfigurationProperties(prefix = "templates")
@PropertySource(value = {"classpath:application.yml"})
public class CommonConfig {

    @Value("${projectName}")
    private String projectName;


    @Value("${groupId}")
    private String groupId;

    /**
     * 作者
     */
    @Value("${author}")
    public String author;

    /**
     * 生成包路径
     */
    @Value("${packageName}")
    public String packageName;

}
