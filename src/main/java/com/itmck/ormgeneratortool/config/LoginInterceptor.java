package com.itmck.ormgeneratortool.config;

import cn.dev33.satoken.stp.StpUtil;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) throws Exception {

        if (StpUtil.isLogin()) {
            log.info("当前登录人:{}", StpUtil.getLoginId());
            return true;
        } else {
            response.sendRedirect("/login.html");
            return false;
        }
    }

}
