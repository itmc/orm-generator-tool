package com.itmck.ormgeneratortool.enums;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/3 10:26
 **/
public enum CommonEnum {

    ERROR_401("401", "用户名或者密码错误"),

    ERROR_402("402", "用已经存在"),

    ERROR_500("500", "系统异常"),
    ERROR_555("555", "已经存在当前表"),
    ERROR_406("406", "未查询到表信息"),
    ERROR_407("407", "未查询到表对应的列信息"),
    ERROR_200("200", "成功"),

    ERROR_600("600", "创建文件目录失败"),
    ERROR_601("601", "当前文件信息不存在"),

    ;

    private final String code;

    private final String description;

    CommonEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}