package com.itmck.ormgeneratortool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 她似晚霞，愈晚愈浓
 * <p>
 * Create by M 347451331@qq.com 2022/7/29 13:34
 **/
@EnableSwagger2
@SpringBootApplication
public class OrmGeneratorToolApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrmGeneratorToolApplication.class, args);
    }

}
