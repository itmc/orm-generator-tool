package com.itmck.ormgeneratortool.response;

import com.itmck.ormgeneratortool.enums.CommonEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/18 14:12
 **/
@Data
@Accessors(chain = true)
public class ApiResultPage<T> {


    /**
     * 当前页数据
     */
    private List<T> list;

    /**
     * 总数
     */
    private long total;

    /**
     * 错误信息
     */
    private String message;

    /**
     * 错误码
     */
    private String errorCode;


    /**
     * 是否成功
     */
    private boolean success = true;


    public static <T> ApiResultPage<T> ok(List<T> list, long total) {
        return new ApiResultPage<T>()
                .setList(list)
                .setTotal(total)
                .setSuccess(true)
                .setErrorCode(CommonEnum.ERROR_200.getCode());
    }

}