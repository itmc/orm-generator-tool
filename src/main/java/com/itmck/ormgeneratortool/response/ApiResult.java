package com.itmck.ormgeneratortool.response;

import com.itmck.ormgeneratortool.enums.CommonEnum;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/3 10:17
 **/
@Data
@Builder
public class ApiResult<T> implements Serializable {

    /**
     * 错误信息
     */
    private String message;

    /**
     * 错误码
     */
    public String errorCode;

    /**
     * 是否成功
     */
    private boolean success;

    /**
     * 响应体
     */
    private T result;

    public ApiResult() {
    }

    public ApiResult(String message, String errorCode, boolean success, T result) {
        this.message = message;
        this.errorCode = errorCode;
        this.success = success;
        this.result = result;
    }

    public static <T> ApiResult<T> ok() {
        return new ApiResult<>("ok", "200", true, null);
    }

    public static <T> ApiResult<T> ok(T data) {
        return new ApiResult<>("ok", "200", true, data);
    }

    public static <T> ApiResult<T> ok(String message, T data) {
        return new ApiResult<>(message, "200", true, data);
    }

    public static <T> ApiResult<T> error(String message, String errorCode) {
        return new ApiResult<>(message, errorCode, false, null);
    }

    public static <T> ApiResult<T> error(CommonEnum commonEnum) {
        return error(commonEnum.getDescription(),commonEnum.getCode());
    }
}