package com.itmck.ormgeneratortool.service;

import com.itmck.ormgeneratortool.dao.entity.LcProjectInfo;
import com.itmck.ormgeneratortool.response.ApiResult;

import javax.servlet.http.HttpServletResponse;

/**
 * 她似晚霞，愈晚愈浓
 * <p>
 * Create by M ChangKe 2022/7/31 10:17
 */
public interface GeneratorService {

    /**
     * 创建springboot应用
     *
     * @param lcProjectInfo 入参
     * @return 创建结果
     */
    ApiResult<String> createSimpleSystem(LcProjectInfo lcProjectInfo);

    void downSimpleSystem( String  projectName, HttpServletResponse response);
}
