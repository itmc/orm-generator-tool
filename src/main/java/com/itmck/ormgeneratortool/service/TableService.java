package com.itmck.ormgeneratortool.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itmck.ormgeneratortool.dao.entity.LcTable;
import com.itmck.ormgeneratortool.dao.entity.TableInformation;
import com.itmck.ormgeneratortool.dto.LcTableDTO;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.response.ApiResultPage;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface TableService extends IService<LcTable> {
    /**
     * 新增表信息
     *
     * @param lcTableDTO
     * @return
     */
    ApiResult<String> insertTable(LcTableDTO lcTableDTO);


    /**
     * 删除表信息
     *
     * @param id
     * @return
     */
    ApiResult<String> deleteTable(Long id);

    /**
     * 更新表信息
     *
     * @param lcTableDTO
     * @return
     */
    ApiResult<String> updateTable(LcTableDTO lcTableDTO);


    /**
     * 查询表信息列表
     *
     * @param request
     * @return
     */
    ApiResultPage<LcTableDTO> queryTableList(HttpServletRequest request);

    /**
     * 批量导入
     *
     * @param file
     * @return
     */
    ApiResult<String> importTable(MultipartFile file);

    ApiResult<List<TableInformation>> queryCurrentTableList();


    LcTable getLcTable(String tableName);
}
