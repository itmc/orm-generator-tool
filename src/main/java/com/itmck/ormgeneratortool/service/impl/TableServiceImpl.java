package com.itmck.ormgeneratortool.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itmck.ormgeneratortool.dao.entity.LcTable;
import com.itmck.ormgeneratortool.dao.entity.TableInformation;
import com.itmck.ormgeneratortool.dao.mapper.LcTableMapper;
import com.itmck.ormgeneratortool.dto.ExcelLcTable;
import com.itmck.ormgeneratortool.dto.LcTableDTO;
import com.itmck.ormgeneratortool.enums.CommonEnum;
import com.itmck.ormgeneratortool.exception.BusinessException;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.response.ApiResultPage;
import com.itmck.ormgeneratortool.service.TableService;
import com.itmck.ormgeneratortool.util.EasyExcelUtil;
import com.itmck.ormgeneratortool.util.MyQueryPageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;


@Slf4j
@Service
public class TableServiceImpl extends ServiceImpl<LcTableMapper, LcTable> implements TableService {

    @Override
    public ApiResultPage<LcTableDTO> queryTableList(HttpServletRequest request) {
        log.info("查询待生成的表对应的列表");
        return MyQueryPageUtil.buildPage(request, this.baseMapper, LcTableDTO.class);
    }

    @Override
    public ApiResult<String> insertTable(LcTableDTO lcTableDTO) {
        LcTable lcTable = BeanUtil.copyProperties(lcTableDTO, LcTable.class);
        this.baseMapper.insert(lcTable);
        return ApiResult.ok("新增成功");
    }

    @Override
    public ApiResult<String> updateTable(LcTableDTO lcTableDTO) {
        LcTable lcTable = BeanUtil.copyProperties(lcTableDTO, LcTable.class);
        this.baseMapper.updateById(lcTable);
        return ApiResult.ok("新增成功");

    }

    @Override
    public ApiResult<String> deleteTable(Long id) {
        this.baseMapper.deleteById(id);
        return ApiResult.ok("成功删除");
    }

    @Override
    public ApiResult<String> importTable(MultipartFile file) {

        try (
                InputStream inputStream = file.getInputStream()
        ) {
            List<ExcelLcTable> excelLcTables = EasyExcelUtil.syncReadModel(inputStream, ExcelLcTable.class);
            List<LcTable> collect = excelLcTables
                    .stream()
                    .map(item -> BeanUtil.copyProperties(item, LcTable.class))
                    .collect(Collectors.toList());
            this.saveBatch(collect);
        } catch (Exception e) {
            log.error("读取文件失败");
            return ApiResult.error("读取文件失败", "500");
        }
        return ApiResult.ok();
    }

    @Override
    public LcTable getLcTable(String tableName) {
        LcTable lcTable = new LambdaQueryChainWrapper<>(this.baseMapper)
                .eq(LcTable::getTableName, tableName)
                .one();
        if (ObjectUtil.isNull(lcTable)) {
            throw new BusinessException(CommonEnum.ERROR_406);
        }
        return lcTable;
    }

    @Override
    public ApiResult<List<TableInformation>> queryCurrentTableList() {

        List<LcTable> list = new LambdaQueryChainWrapper<>(this.baseMapper)
                .list();
        if (CollectionUtil.isNotEmpty(list)) {
            List<TableInformation> collect = list.stream()
                    .map(dto -> BeanUtil.copyProperties(dto, TableInformation.class))
                    .collect(Collectors.toList());
            return ApiResult.ok(collect);
        }
        return ApiResult.ok(null);
    }
}
