package com.itmck.ormgeneratortool.service.impl;

import com.alibaba.fastjson.JSON;
import com.itmck.ormgeneratortool.dao.entity.ColumnInformation;
import com.itmck.ormgeneratortool.dao.entity.TableInformation;
import com.itmck.ormgeneratortool.dao.mapper.ColumnInformationMapper;
import com.itmck.ormgeneratortool.dao.mapper.TableInformationMapper;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.service.DbTableService;
import com.itmck.ormgeneratortool.util.JdbcUrlUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
@Slf4j
public class DbTableServiceImpl implements DbTableService {

    @Resource
    private TableInformationMapper tableInformationMapper;
    @Resource
    private ColumnInformationMapper columnInformationMapper;
    @Resource
    private DataSourceProperties dataSourceProperties;

    @Override
    public ApiResult<List<TableInformation>> queryCurrentTableList() {
        String url = dataSourceProperties.getUrl();
        String dataBaseNameByUrl = JdbcUrlUtil.findDataBaseNameByUrl(url);
        List<TableInformation> tableInformation = tableInformationMapper.showTableInformationList(dataBaseNameByUrl);
        log.info("result:{}", JSON.toJSONString(tableInformation));
        return ApiResult.ok(tableInformation);
    }

    @Override
    public ApiResult<List<ColumnInformation>> queryColumnList(String tableName) {
        String url = dataSourceProperties.getUrl();
        String dataBaseNameByUrl = JdbcUrlUtil.findDataBaseNameByUrl(url);
        List<ColumnInformation> columnInformationList = columnInformationMapper.showColumns(tableName, dataBaseNameByUrl);
        log.info("result:{}", JSON.toJSONString(columnInformationList));
        return ApiResult.ok(columnInformationList);
    }
}
