package com.itmck.ormgeneratortool.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.itmck.ormgeneratortool.dao.entity.LcUser;
import com.itmck.ormgeneratortool.dao.mapper.LcUserMapper;
import com.itmck.ormgeneratortool.dao.mapper.LcUserRoleMapper;
import com.itmck.ormgeneratortool.dto.LcUserDTO;
import com.itmck.ormgeneratortool.dto.LcUserInfoDTO;
import com.itmck.ormgeneratortool.enums.CommonEnum;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.service.LcUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

@Slf4j
@Service
public class LcUserServiceImpl implements LcUserService {

    @Resource
    private LcUserMapper lcUserMapper;


    @Override
    public ApiResult<String> register(LcUser lcUser) {
        if (Optional.ofNullable(new LambdaQueryChainWrapper<>(lcUserMapper)
                .eq(LcUser::getUserName, lcUser.getUserName())
                .one()).isPresent()) {
            return ApiResult.error(CommonEnum.ERROR_402);
        }
        lcUserMapper.insert(lcUser);
        return ApiResult.ok();
    }

    @Override
    public ApiResult<String> checkCurrentUser(String username, String password) {
        LcUser one = new LambdaQueryChainWrapper<>(lcUserMapper).eq(LcUser::getUserName, username).eq(LcUser::getStatus, 1).one();
        if (ObjectUtil.isNotNull(one)) {
            String pwd = one.getPassword();
            if (StrUtil.equals(pwd, password)) {
                StpUtil.login(username);
                return ApiResult.ok();
            }
        }
        return ApiResult.error(CommonEnum.ERROR_401);
    }

    @Override
    public LcUserDTO queryUserInfoByUserId(Long userId) {


        LcUser one = new LambdaQueryChainWrapper<>(lcUserMapper).eq(LcUser::getUserId, userId).one();
        return ObjectUtil.isNotNull(one) ? BeanUtil.copyProperties(one, LcUserDTO.class) : null;
    }

    @Override
    public ApiResult<LcUserInfoDTO> queryUserRoleAndPermissions(String uerName) {

        LcUserInfoDTO lcUserInfoDTO = lcUserMapper.queryUserRoleAndPermissions(uerName);
        return ApiResult.ok(lcUserInfoDTO);
    }
}
