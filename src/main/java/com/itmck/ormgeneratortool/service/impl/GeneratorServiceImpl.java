package com.itmck.ormgeneratortool.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.itmck.ormgeneratortool.dao.entity.LcProjectInfo;
import com.itmck.ormgeneratortool.enums.CommonEnum;
import com.itmck.ormgeneratortool.exception.BusinessException;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.service.FastBuildService;
import com.itmck.ormgeneratortool.service.GeneratorService;
import com.itmck.ormgeneratortool.util.FileDirUtil;
import com.itmck.ormgeneratortool.util.FileZipUtil;
import com.itmck.ormgeneratortool.util.HttpFileUtil;
import com.itmck.ormgeneratortool.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.nio.charset.Charset;
import java.util.Map;

@Slf4j
@Service
public class GeneratorServiceImpl implements GeneratorService {

    @Value("${generator.temp:E:\\}")
    private String nasFile;

    @Resource
    private FastBuildService fastBuildService;


    @Override
    public void downSimpleSystem(String projectName, HttpServletResponse response) {
        String fileDir = nasFile + projectName + FileDirUtil.ZIP;
        File file = new File(fileDir);
        String filePath = nasFile + projectName;
        File file1 = new File(filePath);
        FileZipUtil.zipFiles(new File(filePath), file);
        HttpFileUtil.downLoadFile(fileDir, response, projectName + FileDirUtil.ZIP);
        FileUtil.del(file);
        FileUtil.del(file1);
    }

    @Override
    public ApiResult<String> createSimpleSystem(LcProjectInfo lcProjectInfo) {
        Map<String, Map<String, String>> mps = fastBuildService.createSimpleSystem(lcProjectInfo);
        String project = nasFile + lcProjectInfo.getProjectName();
        String javaDir = project + FileDirUtil.JAVAFILE;
        String javaTestDir = project + FileDirUtil.JAVA_TEST;
        String projectNamePackage = StringUtils.toCamelCaseMid(lcProjectInfo.getProjectName()).toLowerCase();
        String resourcesDir = project + FileDirUtil.RESOURCES;
        mps.forEach((tableName, mptValue) -> mptValue.forEach((key, value) -> extracted(lcProjectInfo, project, javaDir, javaTestDir, projectNamePackage, resourcesDir, tableName, key, value)));
        return ApiResult.ok();

    }

    private void extracted(LcProjectInfo lcProjectInfo, String project, String javaDir, String javaTestDir, String projectNamePackage, String resourcesDir, String tableName, String key, String value) {
        String desc, className;
        switch (key) {
            case "vm/lc/application.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage;
                className = StringUtils.FirstStrUpperCase(StringUtils.toCamelCaseMid(lcProjectInfo.getProjectName())) + "Application.java";
                break;

            case "vm/lc/applicationTest.java.vm":
                desc = javaTestDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage;
                className = StringUtils.FirstStrUpperCase(StringUtils.toCamelCaseMid(lcProjectInfo.getProjectName())) + "ApplicationTest.java";
                break;

            case "vm/lc/controller.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.CONTROLLER;
                className = StringUtils.FirstStrUpperCase(tableName) + "Controller.java";
                break;

            case "vm/lc/service.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.SERVICE;
                className = StringUtils.FirstStrUpperCase(tableName) + "Service.java";
                break;

            case "vm/lc/serviceImpl.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.SERVICE_IMPL;
                className = StringUtils.FirstStrUpperCase(tableName) + "ServiceImpl.java";
                break;

            case "vm/lc/domain.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.ENTITY;
                className = StringUtils.FirstStrUpperCase(tableName) + ".java";
                break;

            case "vm/lc/domainDto.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.DTO;
                className = StringUtils.FirstStrUpperCase(tableName) + "DTO.java";
                break;
            case "vm/lc/mapper.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.MAPPER;
                className = StringUtils.FirstStrUpperCase(tableName) + "Mapper.java";
                break;
            case "vm/lc/mapper.xml.vm":
                desc = resourcesDir + FileDirUtil.MAPPER_XML;
                className = StringUtils.FirstStrUpperCase(tableName) + "Mapper.xml";
                break;

            case "vm/lc/application.yml.vm":
                desc = resourcesDir;
                className = "application.yml";
                break;

            case "vm/lc/ApiResult.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.RESPONSE;
                className = "ApiResult.java";
                break;

            case "vm/lc/ApiResultPage.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.RESPONSE;
                className = "ApiResultPage.java";
                break;

            case "vm/lc/BusinessException.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.EXCEPTION;
                className = "BusinessException.java";
                break;

            case "vm/lc/GlobalExceptionAdvice.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.EXCEPTION;
                className = "GlobalExceptionAdvice.java";
                break;

            case "vm/lc/CommonEnum.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.ENUMS;
                className = "CommonEnum.java";
                break;

            case "vm/lc/EasyExcelUtil.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.UTIL;
                className = "EasyExcelUtil.java";
                break;

            case "vm/lc/HttpFileUtil.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.UTIL;
                className = "HttpFileUtil.java";
                break;

            case "vm/lc/MyQueryPageUtil.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.UTIL;
                className = "MyQueryPageUtil.java";
                break;

            case "vm/lc/ReflectUtil.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.UTIL;
                className = "ReflectUtil.java";
                break;

            case "vm/lc/RequestUtil.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.UTIL;
                className = "RequestUtil.java";
                break;

            case "vm/lc/SaTokenConfigure.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.CONFIG;
                className = "SaTokenConfigure.java";
                break;


            case "vm/lc/LoginInterceptor.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.CONFIG;
                className = "LoginInterceptor.java";
                break;

            case "vm/lc/MybatisPlusConfig.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.CONFIG;
                className = "MybatisPlusConfig.java";
                break;

            case "vm/lc/StpInterfaceComp.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.COMPONENT;
                className = "StpInterfaceComp.java";
                break;

            case "vm/lc/LoginController.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.CONTROLLER;
                className = "LoginController.java";
                break;

            case "vm/lc/LoginService.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.SERVICE;
                className = "LoginService.java";
                break;
            case "vm/lc/LoginServiceImpl.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.SERVICE_IMPL;
                className = "LoginServiceImpl.java";
                break;

            case "vm/lc/ViewController.java.vm":
                desc = javaDir + lcProjectInfo.getGroupId() + File.separatorChar + projectNamePackage + FileDirUtil.CONTROLLER;
                className = "ViewController.java";
                break;

            case "vm/views/about.html.vm":
                desc = resourcesDir + FileDirUtil.TEMPLATES;
                className = "about.html";
                break;

            case "vm/views/index.html.vm":
                desc = resourcesDir + FileDirUtil.TEMPLATES;
                className = "index.html";
                break;

            case "vm/views/login.html.vm":
                desc = resourcesDir + FileDirUtil.STATICDIR;
                className = "login.html";
                break;

            case "vm/views/register.html.vm":
                desc = resourcesDir + FileDirUtil.STATICDIR;
                className = "register.html";
                break;

            case "vm/views/self.html.vm":
                desc = resourcesDir + FileDirUtil.TEMPLATES;
                className = "self.html";
                break;

            case "vm/views/editTable.html.vm":
                desc = resourcesDir + FileDirUtil.TEMPLATES;
                className = tableName + "editTable.html";
                break;
            case "vm/views/table.html.vm":
                desc = resourcesDir + FileDirUtil.TEMPLATES;
                className = tableName + "table.html";
                break;

            case "vm/lc/pom.xml.vm":
                desc = project;
                className = "pom.xml";
                break;


            default: {
                throw new BusinessException(CommonEnum.ERROR_601);
            }
        }
        desc = StrUtil.replace(desc, ".", "/");
        File file = new File(desc + File.separatorChar + className);
        log.info("{}", desc + File.separatorChar + className);
        if (!file.exists()) {
            FileUtil.writeString(value, file, Charset.defaultCharset());
        } else if (lcProjectInfo.isOverwrite()) {
            if (file.delete()) {
                FileUtil.writeString(value, file, Charset.defaultCharset());
            }
        }
    }
}
