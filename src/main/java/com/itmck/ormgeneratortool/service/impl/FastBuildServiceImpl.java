package com.itmck.ormgeneratortool.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.itmck.ormgeneratortool.config.CommonConfig;
import com.itmck.ormgeneratortool.dao.entity.LcProjectInfo;
import com.itmck.ormgeneratortool.dto.LcPkColumn;
import com.itmck.ormgeneratortool.dto.TemplateTable;
import com.itmck.ormgeneratortool.dto.TemplateTableColumn;
import com.itmck.ormgeneratortool.dao.entity.LcTable;
import com.itmck.ormgeneratortool.dao.entity.LcTableColumn;
import com.itmck.ormgeneratortool.dao.mapper.LcTableColumnMapper;
import com.itmck.ormgeneratortool.dao.mapper.LcTableMapper;
import com.itmck.ormgeneratortool.enums.CommonEnum;
import com.itmck.ormgeneratortool.exception.BusinessException;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.service.FastBuildService;
import com.itmck.ormgeneratortool.service.TableService;
import com.itmck.ormgeneratortool.util.GenUtils;
import com.itmck.ormgeneratortool.util.StringUtils;
import com.itmck.ormgeneratortool.util.VelocityInitializer;
import com.itmck.ormgeneratortool.util.VelocityUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.StringWriter;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/**
 * 原：岁华空冉冉，心曲且悠悠
 * 译：即使世界偶尔薄凉，内心也要繁花似锦。浅喜，静爱。深深懂得，淡淡释怀，望远处的是风景，看近处的才是人生。
 * <p>
 * Create by M ChangKe 2022/8/4 22:38
 */
@Slf4j
@Service
public class FastBuildServiceImpl implements FastBuildService {
    @Resource
    private LcTableColumnMapper lcTableColumnMapper;

    @Resource
    private TableService tableService;
    @Resource
    private CommonConfig commonConfig;

    @Override
    public Map<String, String> buildOneTable(LcTable lcTable) {
        String tableName = lcTable.getTableName();
        TemplateTable templateTable = new TemplateTable();
        templateTable
                .setProjectName(commonConfig.getProjectName())
                .setGroupId(commonConfig.getGroupId())
                .setPackageName(commonConfig.getPackageName())
                .setTableComment(lcTable.getTableComment())
                .setFunctionAuthor(commonConfig.getAuthor())
                .setClassName(StringUtils.FirstStrUpperCase(tableName))
                .setModuleName(tableName)
                .setTableName(tableName)
                .setFunctionName(tableName)
                .setBusinessName(tableName);
        return buildOneTable(templateTable);
    }

    public Map<String, String> buildOneTable(TemplateTable templateTable) {

        LcTable lcTable = tableService.getLcTable(templateTable.getTableName());
        List<LcTableColumn> list = new LambdaQueryChainWrapper<>(lcTableColumnMapper)
                .eq(LcTableColumn::getTableId, lcTable.getTableId())
                .list();
        if (CollectionUtil.isEmpty(list)) {
            throw new BusinessException(CommonEnum.ERROR_407);
        }
        List<TemplateTableColumn> columns = new ArrayList<>();
        list.forEach(dto -> {
            TemplateTableColumn templateTableColumn = new TemplateTableColumn();
            templateTableColumn.setColumnName(dto.getColumnName())
                    .setIsPk(dto.getIsPk())
                    .setColumnType(dto.getColumnType())
                    .setColumnComment(dto.getColumnComment())
                    .setJavaField(StringUtils.toCamelCase(dto.getColumnName()))
            ;
            GenUtils.initColumnField(templateTableColumn, templateTable);
            columns.add(templateTableColumn);
            if (StrUtil.equals(dto.getIsPk(), "1")) {
                LcPkColumn lcPkColumn = new LcPkColumn();
                lcPkColumn.setJavaType(dto.getJavaType())
                        .setJavaField(dto.getJavaField())
                        .setColumnName(dto.getColumnName())
                        .setColumnComment(dto.getColumnComment())
                ;
                templateTable.setLcPkColumn(lcPkColumn);
            }
        });
        templateTable.setTableComment(lcTable.getTableComment())
                .setColumns(columns);
        return buildTemplate(templateTable);

    }


    private HashMap<String, String> buildTemplate(TemplateTable table) {
        VelocityInitializer.initVelocity();
        List<String> templates = VelocityUtils.getTemplateList(table);
        HashMap<String, String> map = new HashMap<>();
        for (String template : templates) {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, "utf-8");
            VelocityContext context = VelocityUtils.prepareContext(table);
            tpl.merge(context, sw);
            map.put(template, sw.toString());
        }
        return map;
    }

    @Override
    public Map<String, Map<String, String>> createSimpleSystem(LcProjectInfo lcProjectInfo) {
        String tableStr = lcProjectInfo.getTableStr();
        return createSystemByTable(lcProjectInfo, tableStr, s -> Arrays.asList(tableStr.split(",")));
    }

    public Map<String, Map<String, String>> createSystemByTable(LcProjectInfo lcProjectInfo, String tableStr, Function<String, List<String>> function) {
        List<String> tables = strToList(tableStr, function);
        List<String> tables1 = lcProjectInfo.getTables();
        List<String> collect = new ArrayList<>(CollectionUtil.union(tables, tables1));
        if (CollectionUtil.isEmpty(collect)) {
            return createSystemNulTable(lcProjectInfo);
        }
        return createSystemHavTable(lcProjectInfo, collect);
    }

    private Map<String, Map<String, String>> createSystemHavTable(LcProjectInfo lcProjectInfo, List<String> tables) {
        Map<String, Map<String, String>> mmp = new HashMap<>();
        tables.forEach(tableName -> mmp.put(tableName, contTemplate(lcProjectInfo, dto -> getTemplateTable(new TemplateTable()
                .setHaveTables(true)
                .setTableName(tableName)
                .setFunctionAuthor(lcProjectInfo.getAuthor())
                .setModuleName(tableName)
                .setClassName(StringUtils.FirstStrUpperCase(tableName))
                .setFunctionName(tableName)
                .setBusinessName(tableName), lcProjectInfo))));
        return mmp;
    }

    private TemplateTable getTemplateTable(TemplateTable tableName, LcProjectInfo lcProjectInfo) {
        return tableName
                .setProjectName(lcProjectInfo.getProjectName())
                .setGroupId(lcProjectInfo.getGroupId())
                .setPackageName(lcProjectInfo.getGroupId())
                .setCreatePre(lcProjectInfo.isCreatePre())
                .setLoginPage(lcProjectInfo.isLoginPage())
                .setOpenSwagger(lcProjectInfo.isOpenSwagger())
                .setOpenSaToken(lcProjectInfo.isOpenSaToken());
    }

    @NotNull
    private Map<String, Map<String, String>> createSystemNulTable(LcProjectInfo lcProjectInfo) {
        log.info("前端未传入表,直接生成基础springboot应用,{}", JSON.toJSONString(lcProjectInfo));
        Map<String, Map<String, String>> mmp = new HashMap<>();
        mmp.put("simple", contTemplate(lcProjectInfo, dto -> getTemplateTable(new TemplateTable()
                .setHaveTables(false)
                .setFunctionAuthor(lcProjectInfo.getAuthor()), lcProjectInfo)));
        return mmp;
    }


    public List<String> strToList(String t, Function<String, List<String>> function) {
        if (StringUtils.isEmpty(t)) {
            return null;
        }
        return function.apply(t);
    }

    public Map<String, String> contTemplate(LcProjectInfo lcProjectInfo, Function<LcProjectInfo, TemplateTable> function) {

        if (ObjectUtil.isNotEmpty(lcProjectInfo)) {
            return buildOneTable(function.apply(lcProjectInfo));
        }
        return null;

    }
}
