package com.itmck.ormgeneratortool.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itmck.ormgeneratortool.dao.entity.LcTable;
import com.itmck.ormgeneratortool.dao.entity.LcTableColumn;
import com.itmck.ormgeneratortool.dao.mapper.LcTableColumnMapper;
import com.itmck.ormgeneratortool.dao.mapper.LcTableMapper;
import com.itmck.ormgeneratortool.dto.LcTableColumnDTO;
import com.itmck.ormgeneratortool.dto.ExcelLcTableColumn;
import com.itmck.ormgeneratortool.enums.CommonEnum;
import com.itmck.ormgeneratortool.exception.BusinessException;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.response.ApiResultPage;
import com.itmck.ormgeneratortool.service.ColumnService;
import com.itmck.ormgeneratortool.util.EasyExcelUtil;
import com.itmck.ormgeneratortool.util.MyQueryPageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ColumnServiceImpl extends ServiceImpl<LcTableColumnMapper, LcTableColumn> implements ColumnService {


    @Resource
    private LcTableColumnMapper lcTableColumnMapper;

    @Resource
    private LcTableMapper lcTableMapper;


    @Override
    public ApiResultPage<LcTableColumnDTO> queryColumnList(HttpServletRequest request) {

        Page<LcTableColumn> page = MyQueryPageUtil.builder(request, lcTableColumnMapper);
        List<LcTableColumnDTO> collect = page.getRecords()
                .stream()
                .map(item -> BeanUtil.copyProperties(item, LcTableColumnDTO.class))
                .collect(Collectors.toList());

        collect.forEach(dto -> Optional.ofNullable(new LambdaQueryChainWrapper<>(lcTableMapper)
                .eq(LcTable::getTableId, dto.getTableId())
                .one()).ifPresent(lcTable -> dto.setTableIdZh(lcTable.getTableName())));

        return ApiResultPage.ok(collect, page.getTotal());
    }

    @Override
    public ApiResult<String> insertColumn(LcTableColumnDTO lcTableColumnDTO) {

        LcTableColumn lcTableColumn = BeanUtil.copyProperties(lcTableColumnDTO, LcTableColumn.class);
        lcTableColumnMapper.insert(lcTableColumn);

        return ApiResult.ok("新增成功");
    }


    @Override
    public ApiResult<String> deleteColumn(Long columnId) {
        lcTableColumnMapper.deleteById(columnId);
        return ApiResult.ok("删除成功");
    }

    @Override
    public ApiResult<String> updateColumn(LcTableColumnDTO lcTableColumnDTO) {

        LcTableColumn lcTableColumn = BeanUtil.copyProperties(lcTableColumnDTO, LcTableColumn.class);
        lcTableColumnMapper.updateById(lcTableColumn);
        return ApiResult.ok("更新成功");
    }

    @Override
    public ApiResult<String> importTable(MultipartFile file) {
        try (
                InputStream inputStream = file.getInputStream()
        ) {
            List<ExcelLcTableColumn> lcTableExcels = EasyExcelUtil.syncReadModel(inputStream, ExcelLcTableColumn.class);
            List<LcTableColumn> collect = lcTableExcels
                    .stream()
                    .map(item -> BeanUtil.copyProperties(item, LcTableColumn.class))
                    .collect(Collectors.toList());
            this.saveBatch(collect);
        } catch (Exception e) {
            log.error("读取文件失败");
            return ApiResult.error("读取文件失败", "500");
        }
        return ApiResult.ok();
    }

    @Override
    public ApiResult<String> addColumnList(List<LcTableColumnDTO> list) {


        LcTable lcTable = new LcTable();
        list.stream().findFirst().ifPresent(dto -> {
            Optional.ofNullable(new LambdaQueryChainWrapper<>(lcTableMapper)
                    .eq(LcTable::getTableName, dto.getTableName())
                    .one()).ifPresent(ddo -> {
                throw new BusinessException(CommonEnum.ERROR_555);
            });
            lcTable.setTableName(dto.getTableName());
            lcTable.setTableComment(dto.getTableComment());
            lcTableMapper.insertLcTable(lcTable);
        });

        log.info("lcTableId:{}", lcTable.getTableId());
        List<LcTableColumn> collect = list
                .stream()
                .map(item -> {
                    LcTableColumn lcTableColumn = BeanUtil.copyProperties(item, LcTableColumn.class);
                    lcTableColumn.setTableId(lcTable.getTableId() + "");
                    return lcTableColumn;
                })
                .collect(Collectors.toList());
        this.saveBatch(collect);
        return ApiResult.ok();
    }
}
