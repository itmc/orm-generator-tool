package com.itmck.ormgeneratortool.service;

import com.itmck.ormgeneratortool.dao.entity.LcProjectInfo;
import com.itmck.ormgeneratortool.dao.entity.LcTable;
import com.itmck.ormgeneratortool.dto.TemplateTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 代码生成接口
 * <p>
 * 太阳当空照，花儿对我笑
 * <p>
 * Create by M ChangKe 2022/7/28 22:27
 */
public interface FastBuildService {

    /**
     * 对接前台单表生生对应的代码
     *
     * @param lcTable 表
     * @return 代码
     */
    Map<String, String> buildOneTable(LcTable lcTable);

    /**
     * 一键创建系统
     *
     * @param lcProjectInfo 前端创建系统入参
     * @return 代码
     */
    Map<String, Map<String, String>> createSimpleSystem(LcProjectInfo lcProjectInfo);
}