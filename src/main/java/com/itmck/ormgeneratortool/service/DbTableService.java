package com.itmck.ormgeneratortool.service;

import com.itmck.ormgeneratortool.dao.entity.ColumnInformation;
import com.itmck.ormgeneratortool.dao.entity.TableInformation;
import com.itmck.ormgeneratortool.response.ApiResult;

import java.util.List;

public interface DbTableService {
    ApiResult<List<TableInformation>> queryCurrentTableList();

    ApiResult<List<ColumnInformation>> queryColumnList(String tableName);
}
