package com.itmck.ormgeneratortool.service;

import com.itmck.ormgeneratortool.dao.entity.LcUser;
import com.itmck.ormgeneratortool.dto.LcUserDTO;
import com.itmck.ormgeneratortool.dto.LcUserInfoDTO;
import com.itmck.ormgeneratortool.response.ApiResult;

public interface LcUserService {


    /**
     * 用户登录验证
     * @param username
     * @param password
     * @return
     */
    ApiResult<String> checkCurrentUser(String username, String password);

    /**
     * 根据用户id查询用户信息
     *
     * @param userId
     * @return
     */
    LcUserDTO queryUserInfoByUserId(Long userId);

    ApiResult<LcUserInfoDTO> queryUserRoleAndPermissions(String userId);

    /**
     * 用户注册
     * @param lcUser
     * @return
     */
    ApiResult<String> register(LcUser lcUser);
}
