package com.itmck.ormgeneratortool.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itmck.ormgeneratortool.dao.entity.LcTableColumn;
import com.itmck.ormgeneratortool.dto.LcTableColumnDTO;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.response.ApiResultPage;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ColumnService  extends IService<LcTableColumn> {


    ApiResult<String> insertColumn(LcTableColumnDTO lcTableColumnDTO);

    ApiResultPage<LcTableColumnDTO> queryColumnList(HttpServletRequest request);

    ApiResult<String> deleteColumn(Long columnId);

    ApiResult<String> updateColumn(LcTableColumnDTO lcTableColumnDTO);

    ApiResult<String> importTable(MultipartFile file);

    ApiResult<String> addColumnList(List<LcTableColumnDTO> list);
}
