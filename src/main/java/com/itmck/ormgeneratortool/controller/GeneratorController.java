package com.itmck.ormgeneratortool.controller;


import com.alibaba.fastjson.JSONObject;
import com.itmck.ormgeneratortool.dao.entity.LcProjectInfo;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.service.GeneratorService;
import com.itmck.ormgeneratortool.util.FileDirUtil;
import com.itmck.ormgeneratortool.util.FileZipUtil;
import com.itmck.ormgeneratortool.util.HttpFileUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * 山河远阔,人间烟火,无一是你,无一不是你
 * <p>
 * Create by M ChangKe 2022/7/28 15:59
 **/
@Slf4j
@RestController
@RequestMapping("/create")
@Api(value = "GeneratorController", tags = "GeneratorController")
public class GeneratorController {
    @Resource
    private GeneratorService generatorService;

    @PostMapping("/system")
    @ApiOperation(value = "创建系统", notes = "创建系统")
    public ApiResult<String> createSimpleSystem(@RequestBody LcProjectInfo lcProjectInfo) {
        log.info("创建简单系统,createSimpleSystem()，入参：{}", JSONObject.toJSONString(lcProjectInfo));
        return generatorService.createSimpleSystem(lcProjectInfo);
    }

    @GetMapping("/down")
    @ApiOperation(value = "下载系统", notes = "下载系统")
    public void downSimpleSystem(@RequestParam String projectName, HttpServletResponse response) {
        log.info("下载系统,downSimpleSystem()，projectName:{}", projectName);
        generatorService.downSimpleSystem(projectName, response);
    }
}
