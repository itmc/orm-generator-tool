package com.itmck.ormgeneratortool.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.itmck.ormgeneratortool.dao.entity.LcTable;
import com.itmck.ormgeneratortool.service.FastBuildService;
import com.itmck.ormgeneratortool.service.TableService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.Map;


/**
 * 当前控制器主要进行页面之间的跳转动作
 * <p>
 * Create by M ChangKe 2022/7/14 22:44
 **/
@Slf4j
@Controller
public class ViewController {


    @Resource
    private TableService tableService;
    @Resource
    private FastBuildService fastBuildService;

    @GetMapping("/index")
    public String gen() {
        log.info("跳转到首页");
        return "index";
    }

    @SaCheckLogin
    @RequestMapping("self")
    public String main() {
        log.info("跳转到主页面");
        return "self";
    }

    @SaCheckLogin
    @RequestMapping("about")
    public String about() {
        log.info("跳转到关于页面");
        return "about";
    }

    @SaCheckLogin
    @RequestMapping("addTable")
    public String addTable() {
        log.info("跳转到新增表");
        return "addTable";
    }


    @SaCheckLogin
    @RequestMapping("tableColumn")
    public String tableColumn() {
        log.info("跳转到表字段信息管理页");
        return "tableColumn";
    }

    @SaCheckLogin
    @RequestMapping("table")
    public String table() {
        log.info("跳转到表信息管理页");
        return "table";
    }

    @SaCheckLogin
    @RequestMapping("createTable")
    public String createTable() {
        log.info("跳转到表新增页面");
        return "createTable";
    }

    @SaCheckLogin
    @RequestMapping("editTable")
    public String editTable() {
        log.info("跳转到表编辑页面");
        return "editTable";
    }

    @SaCheckLogin
    @RequestMapping("editTableColumn")
    public String editTableColumn() {
        log.info("跳转到表对应列信息编辑页面");
        return "editTableColumn";
    }

    /**
     * 生成代码并返回到前端进行填充展示
     *
     * @param tableName 表名
     * @param model     model视图
     * @return 代码
     */
    @SaCheckLogin
    @GetMapping("build")
    public String paperInfo(@RequestParam String tableName, Model model) {
        log.info("通过表:{},生成代码", tableName);
        LcTable lcTable = tableService.getLcTable(tableName);
        Map<String, String> map = fastBuildService.buildOneTable(lcTable);
        model.addAttribute("map", map);
        return "all";
    }
}
