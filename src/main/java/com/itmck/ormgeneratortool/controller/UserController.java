package com.itmck.ormgeneratortool.controller;


import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.json.JSON;
import com.alibaba.fastjson.JSONObject;
import com.itmck.ormgeneratortool.dao.entity.LcUser;
import com.itmck.ormgeneratortool.dto.LcUserInfoDTO;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.service.LcUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("user")
@Api(value = "UserController", tags = "用户相关操作")
public class UserController {


    @Resource
    private LcUserService lcUserService;

    @PostMapping("register")
    @ApiOperation(value = "用户注册", notes = "用户注册")
    public ApiResult<String> register(LcUser lcUser){
        log.info("用户登录,当前用户:{}", JSONObject.toJSONString(lcUser));
        return lcUserService.register(lcUser);
    }


    @GetMapping("doLogin")
    @ApiOperation(value = "用户登录", notes = "用户登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "用户名",defaultValue = "mck",dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", defaultValue = "123",dataType = "String", paramType = "query")
    })
    public ApiResult<String> login(@RequestParam String userName, @RequestParam String password) {
        log.info("用户登录,当前用户:{},密码:{}", userName, password);
        return lcUserService.checkCurrentUser(userName, password);
    }

    @GetMapping("isLogin")
    @ApiOperation(value = "查询登录状态", notes = "查询登录状态")
    public String isLogin() {
        log.info("查询当前登录状态");
        if (StpUtil.isLogin()) {
            log.info("当前登录人:{}", StpUtil.getLoginId());
        }
        return StpUtil.isLogin() ? "当前会话已经登录, 登录人: " + StpUtil.getLoginId() : "当前会话未登录:";
    }


    @SaCheckLogin
    @GetMapping("queryUserInfo/{userName}")
    @ApiOperation(value = "查询用户信息", notes = "查询用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "用户名", dataType = "String", paramType = "path")
    })
    public ApiResult<LcUserInfoDTO> queryUserRoleAndPermissions(@PathVariable("userName") String userName) {
        log.info("查询用户信息,当前用户:{}", userName);
        return lcUserService.queryUserRoleAndPermissions(userName);
    }


    @SaCheckLogin//登录认证：只有登录之后才能进入该方法
    @SaCheckRole("za")//角色认证：必须具有指定角色才能进入该方法
    @SaCheckPermission("az")//权限认证：必须具有指定权限才能进入该方法
    @GetMapping("tokenInfo")
    @ApiOperation(value = "查询Token信息", notes = "查询Token信息")
    public ApiResult<SaTokenInfo> tokenInfo() {
        log.info("查询 Token 信息");
        return ApiResult.ok(StpUtil.getTokenInfo());
    }

    @SaCheckLogin
    @GetMapping("logout")
    @ApiOperation(value = "注销", notes = "注销")
    public ApiResult<String> logout() {
        log.info("注销");
        StpUtil.logout();
        return ApiResult.ok();
    }

}
