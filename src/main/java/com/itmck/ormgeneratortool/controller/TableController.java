package com.itmck.ormgeneratortool.controller;

import com.alibaba.fastjson.JSON;
import com.itmck.ormgeneratortool.dto.LcTableDTO;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.response.ApiResultPage;
import com.itmck.ormgeneratortool.service.TableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2022/7/12 15:45
 **/
@Slf4j
@RestController
@RequestMapping("table")
@Api(value = "TableController", tags = "表操作")
public class TableController {

    @Resource
    private TableService tableService;


    @GetMapping("/queryTableList")
    @ApiOperation(value = "查询表信息列表", notes = "查询表信息列表")
    public ApiResultPage<LcTableDTO> queryTableList(HttpServletRequest request) {
        log.info("查询表信息列表,queryTableList()");
        return tableService.queryTableList(request);
    }


    @PostMapping("/insertTable")
    @ApiOperation(value = "新增表信息", notes = "新增表信息")
    public ApiResult<String> insertTable(@RequestBody LcTableDTO lcTableDTO) {
        log.info("新增表信息:{}", JSON.toJSONString(lcTableDTO));
        return tableService.insertTable(lcTableDTO);
    }


    @GetMapping("/deleteTable")
    @ApiOperation(value = "删除表信息", notes = "删除表信息")
    @ApiImplicitParam(name = "tableId", value = "表id", dataType = "Long", paramType = "query")
    public ApiResult<String> deleteTable(@RequestParam Long tableId) {
        log.info("删除表信息,id:{}", tableId);
        return tableService.deleteTable(tableId);
    }


    @PostMapping("/updateTable")
    @ApiOperation(value = "修改表信息", notes = "修改表信息")
    public ApiResult<String> updateTable(@RequestBody LcTableDTO lcTableDTO) {
        log.info("修改表信息:{}", JSON.toJSONString(lcTableDTO));
        return tableService.updateTable(lcTableDTO);
    }


    @ApiOperation(value = "批量导入表信息", notes = "批量导入表信息", consumes = "multipart/form-data")
    @PostMapping("import")
    public ApiResult<String> importTable(@RequestParam("file") MultipartFile file) {
        log.info("当前文件:{}", file.getOriginalFilename());
        return tableService.importTable(file);

    }


}
