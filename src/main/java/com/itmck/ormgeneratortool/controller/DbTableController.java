package com.itmck.ormgeneratortool.controller;


import com.itmck.ormgeneratortool.dao.entity.ColumnInformation;
import com.itmck.ormgeneratortool.dao.entity.TableInformation;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.service.DbTableService;
import com.itmck.ormgeneratortool.service.TableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 她似晚霞，愈晚愈浓
 * <p>
 * Create by M 347451331@qq.com 2022/8/2 14:32
 * <p>
 * 获取当前应用对应的数据库的表信息(表已经存在于数据库)
 **/
@Slf4j
@RestController
@RequestMapping("/current")
@Api(value = "DbTableController", tags = "数据库表")
public class DbTableController {
    @Resource
    private DbTableService dbTableService;
    @Resource
    private TableService tableService;

    /**
     * 返回前端当前系统下的所有表信息
     */
    @GetMapping("/queryTableList")
    @ApiOperation(value = "获取所有的表信息", notes = "获取所有的表信息")
    public ApiResult<List<TableInformation>> queryCurrentTableList() {
        log.info("获取所有的表信息，queryCurrentTableList()");
//        return dbTableService.queryCurrentTableList();
        return tableService.queryCurrentTableList();
    }

    /**
     * 根据表名查询其属性字段信息
     *
     * @param tableName 表名
     * @return 表对应的字段信息
     */
    @GetMapping("/queryColumnList")
    @ApiOperation(value = "查询列信息列表", notes = "查询列信息列表")
    public ApiResult<List<ColumnInformation>> queryColumnList(@RequestParam String tableName) {
        log.info("查询列信息列表，queryColumnList({})", tableName);
        return dbTableService.queryColumnList(tableName);
    }


}
