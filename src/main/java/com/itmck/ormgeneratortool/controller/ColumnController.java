package com.itmck.ormgeneratortool.controller;

import com.alibaba.fastjson.JSON;
import com.itmck.ormgeneratortool.dto.LcTableColumnDTO;
import com.itmck.ormgeneratortool.response.ApiResult;
import com.itmck.ormgeneratortool.response.ApiResultPage;
import com.itmck.ormgeneratortool.service.ColumnService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2022/7/12 15:45
 **/
@Slf4j
@RestController
@RequestMapping("/column")
@Api(value = "ColumnController", tags = "列操作")
public class ColumnController {

    @Resource
    private ColumnService columnService;


    @GetMapping("/queryColumnList")
    @ApiOperation(value = "查询列信息列表", notes = "查询列信息列表")
    public ApiResultPage<LcTableColumnDTO> queryColumnList(HttpServletRequest request) {
        log.info("查询列信息列表，queryColumnList()");
        return columnService.queryColumnList(request);
    }


    @PostMapping("/insertColumn")
    @ApiOperation(value = "新增表字段信息", notes = "新增表字段信息")
    public ApiResult<String> insertColumn(@RequestBody LcTableColumnDTO lcTableColumnDTO) {
        log.info("新增表字段信息:{}", JSON.toJSONString(lcTableColumnDTO));
        return columnService.insertColumn(lcTableColumnDTO);
    }


    @GetMapping("/deleteColumn")
    @ApiOperation(value = "删除表字段信息", notes = "删除表字段信息")
    @ApiImplicitParam(name = "columnId", value = "字段id", dataType = "Long", paramType = "query")
    public ApiResult<String> deleteColumn(@RequestParam Long columnId) {
        log.info("删除表字段信息,id:{}", columnId);
        return columnService.deleteColumn(columnId);
    }


    @PostMapping("/updateColumn")
    @ApiOperation(value = "修改列信息", notes = "修改列信息")
    public ApiResult<String> updateColumn(@RequestBody LcTableColumnDTO lcTableColumnDTO) {
        log.info("修改列信息:{}", JSON.toJSONString(lcTableColumnDTO));
        return columnService.updateColumn(lcTableColumnDTO);
    }


    @ApiOperation(value = "批量导入表对应的列信息", notes = "批量导入表对应的列信息", consumes = "multipart/form-data")
    @PostMapping("import")
    public ApiResult<String> importTable(@RequestParam("file") MultipartFile file) {
        log.info("当前文件:{}", file.getOriginalFilename());
        return columnService.importTable(file);
    }

    @PostMapping("/addColumnList")
    @ApiOperation(value = "新增表", notes = "新增表")
    public ApiResult<String> addColumnList(@RequestBody List<LcTableColumnDTO> list) {
        log.info("批量新增:{}", JSON.toJSONString(list));
        return columnService.addColumnList(list);
    }

}
