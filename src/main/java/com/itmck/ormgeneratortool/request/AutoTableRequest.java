package com.itmck.ormgeneratortool.request;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AutoTableRequest {


    private String tableName;

    private String  tableSchema;
}
