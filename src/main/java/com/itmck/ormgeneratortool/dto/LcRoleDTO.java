package com.itmck.ormgeneratortool.dto;

import lombok.Data;

@Data
public class LcRoleDTO {

    /**
     * 角色
     */
    private Long roleId;

    /**
     * 角色名
     */
    private String roleName;




}
