package com.itmck.ormgeneratortool.dto;


import lombok.Data;

import java.util.List;

@Data
public class LcUserInfoDTO {


    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 角色
     */
    private List<LcRoleDTO> roles;

    /**
     * 权限
     */
    private List<LcPermissionDTO> permissions;
}
