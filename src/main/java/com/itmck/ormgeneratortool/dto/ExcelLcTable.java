package com.itmck.ormgeneratortool.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
public class ExcelLcTable implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 表名称
     */

    @ExcelProperty(value = "表名称", index = 0)
    private String tableName;

    /**
     * 表描述
     */
    @ExcelProperty(value = "表描述", index = 1)
    private String tableComment;


    /**
     * 实体类名称
     */
    @ExcelProperty(value = "实体类名称", index = 2)
    private String className;
}
