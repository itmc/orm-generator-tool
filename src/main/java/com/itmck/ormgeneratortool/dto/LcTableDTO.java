package com.itmck.ormgeneratortool.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;


@Data
@Accessors(chain = true)
public class LcTableDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private Long tableId;

    /**
     * 表名称
     */

    private String tableName;

    /**
     * 表描述
     */
    private String tableComment;


    /**
     * 实体类名称
     */
    private String className;

    /**
     * 表列信息
     */
    private List<LcTableColumnDTO> columns;


}