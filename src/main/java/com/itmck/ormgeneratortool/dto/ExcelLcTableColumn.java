package com.itmck.ormgeneratortool.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
public class ExcelLcTableColumn implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 归属表编号
     */
    @ExcelProperty(value = "归属表编号", index = 0)
    private String tableId;
    /**
     * 列名称
     */
    @ExcelProperty(value = "列名称", index = 1)
    private String columnName;
    /**
     * 列描述
     */
    @ExcelProperty(value = "列描述", index = 2)
    private String columnComment;
    /**
     * 列类型
     */
    @ExcelProperty(value = "tableName", index = 3)
    private String columnType;
    /**
     * JAVA类型
     */
    @ExcelProperty(value = "JAVA类型", index = 4)
    private String javaType;
    /**
     * JAVA字段名
     */
    @ExcelProperty(value = "JAVA字段名", index = 5)
    private String javaField;
    /**
     * 是否主键（1是）
     */
    @ExcelProperty(value = "是否主键（1是）", index = 6)
    private String isPk;
    /**
     * 查询方式（等于、不等于、大于、小于、范围）
     */
    @ExcelProperty(value = "查询方式（等于、不等于、大于、小于、范围）", index = 7)
    private String queryType;
}
