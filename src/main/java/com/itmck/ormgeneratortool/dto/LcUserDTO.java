package com.itmck.ormgeneratortool.dto;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.itmck.ormgeneratortool.dao.entity.LcRole;
import lombok.Data;

import java.util.List;

@Data
@TableName("LC_USER")
public class LcUserDTO {


    /**
     * 用户id
     */
    @TableId(value = "userId", type = IdType.AUTO)
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;


    /**
     * 状态 1:正常,2:锁定
     */
    private Integer status;

    /**
     * 角色
     */
    private List<LcRoleDTO> roles;
}
