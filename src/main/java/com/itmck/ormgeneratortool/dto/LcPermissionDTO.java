package com.itmck.ormgeneratortool.dto;

import lombok.Data;

@Data
public class LcPermissionDTO {

    /**
     * 角色
     */
    private Long permissionId;

    /**
     * 角色名
     */
    private String permissionName;
}
