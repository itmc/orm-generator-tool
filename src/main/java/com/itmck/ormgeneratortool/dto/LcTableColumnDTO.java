package com.itmck.ormgeneratortool.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;


@Data
@Accessors(chain = true)
public class LcTableColumnDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private Long columnId;
    /**
     * 归属表编号
     */
    private String tableId;
    private String tableIdZh;
    private String tableName;
    private String tableComment;

    /**
     * 列名称
     */
    private String columnName;
    /**
     * 列描述
     */
    private String columnComment;
    /**
     * 列类型
     */
    private String columnType;
    /**
     * JAVA类型
     */
    private String javaType;
    /**
     * JAVA字段名
     */
    private String javaField;
    /**
     * 是否主键（1是）
     */
    private String isPk;
    /**
     * 查询方式（等于、不等于、大于、小于、范围）
     */
    private String queryType;


}