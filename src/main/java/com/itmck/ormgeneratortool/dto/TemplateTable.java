package com.itmck.ormgeneratortool.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;


@Data
@Accessors(chain = true)
public class TemplateTable {
    private static final long serialVersionUID = 1L;


    /**
     * 组织
     */
    @NotBlank(message = "组织")
    private String groupId;


    /**
     * 应用
     */
    @NotBlank(message = "应用名")
    private String projectName;


    /**
     * 编号
     */
    private Long tableId;


    /**
     * 表名称
     */
    @NotBlank(message = "表名称不能为空")
    private String tableName;

    /**
     * 表描述
     */
    @NotBlank(message = "表描述不能为空")
    private String tableComment;

    /**
     * 实体类名称(首字母大写)
     */
    @NotBlank(message = "实体类名称不能为空")
    private String className;

    /**
     * 使用的模板（crud单表操作 tree树表操作 sub主子表操作）
     */
    private String tplCategory;

    /**
     * 生成包路径
     */
    @NotBlank(message = "生成包路径不能为空")
    private String packageName;

    /**
     * 生成模块名
     */
    @NotBlank(message = "生成模块名不能为空")
    private String moduleName;

    /**
     * 生成业务名
     */
    @NotBlank(message = "生成业务名不能为空")
    private String businessName;

    /**
     * 生成功能名
     */
    @NotBlank(message = "生成功能名不能为空")
    private String functionName;

    /**
     * 生成作者
     */
    @NotBlank(message = "作者不能为空")
    private String functionAuthor;


    /**
     * 主键
     */
    @NotBlank(message = "主键")
    private String pkName;


    /**
     * true：创建前端
     * false：不创建前端
     */
    private boolean createPre;


    /**
     * 是否创建登录页面
     */
    private boolean loginPage;


    /**
     * 是否使用swagger
     */
    private boolean openSwagger;


    /**
     * 是否使用sa-token
     */
    private boolean openSaToken;


    private LcPkColumn lcPkColumn;

    /**
     * 是否存在表
     */
    private boolean haveTables;

    /**
     * 表列信息
     */
    @Valid
    private List<TemplateTableColumn> columns;


}