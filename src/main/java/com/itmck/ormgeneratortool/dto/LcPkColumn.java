package com.itmck.ormgeneratortool.dto;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LcPkColumn {


    /**
     * 列名称
     */
    private String columnName;
    /**
     * 列描述
     */
    private String columnComment;

    /**
     * java字段
     */
    private String javaField;

    /**
     * java类型
     */
    private String javaType;
}
