# 代码生成器
> 通过表，即可快速生成springboot应用程序。支持自定义模板。

# 项目结构
> 生成的项目结构类似当前项目结构。其中包含了swagger，sa-token，easyExcel等。导入到idea即可启动应用。

```text
├─src
│  ├─main
│  │  ├─java
│  │  │  └─com
│  │  │      └─itmck
│  │  │          └─ormgeneratortool
│  │  │              ├─component
│  │  │              ├─config
│  │  │              ├─constant
│  │  │              ├─controller
│  │  │              ├─dao
│  │  │              │  ├─entity
│  │  │              │  └─mapper
│  │  │              ├─dto
│  │  │              ├─enums
│  │  │              ├─exception
│  │  │              ├─request
│  │  │              ├─response
│  │  │              ├─service
│  │  │              │  └─impl
│  │  │              └─util
│  │  └─resources
│  │      ├─mybatis
│  │      │  └─mapper
│  │      ├─sql
│  │      ├─static
│  │      ├─templates
│  │      └─vm
│  │          ├─default
│  │          ├─lc
│  │          └─views
│  └─test
│      └─java
│          └─com
│              └─itmck
│                  └─ormgeneratortool
```